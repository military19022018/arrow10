﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tumbler2 : MonoBehaviour {
    public int place = 1;

    private void OnMouseDown()
    {
        switch (place)
        {
            case 1:
                transform.Rotate((float)45, 0, 0);
                place++;
                break;
            case 2:
                transform.Rotate((float)-45, 0, 0);
                place++;
                break;
            case 3:
                transform.Rotate((float)-30, 0, 0);
                place++;
                break;
            case 4:
                transform.Rotate((float)30, 0, 0);
                place = 1;
                break;
            default:
                break;

        }
    }
}

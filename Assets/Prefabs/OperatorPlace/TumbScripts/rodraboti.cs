﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rodraboti : MonoBehaviour {

    public int place = 1;
    private void OnMouseDown()
    {
        switch (place)
        {
            case 1:
                transform.Rotate(0, 0, (float)40);
                place++;
                break;
            case 2:
                transform.Rotate(0, 0, (float)50);
                place++;
                break;
            case 3:
                transform.Rotate(0, 0, (float)50);
                place++;
                break;
            case 4:
                transform.Rotate(0, 0, (float)50);
                place++;
                break;
            case 5:
                transform.Rotate(0, 0, (float)-50);
                place++;
                break;
            case 6:
                transform.Rotate(0, 0, (float)-50);
                place++;
                break;
            case 7:
                transform.Rotate(0, 0, (float)-50);
                place++;
                break;
            case 8:
                transform.Rotate(0, 0, (float)-40);
                place = 1;
                break;
        }
    }
}

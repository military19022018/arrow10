﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Assets.Core.Classes.Logic;
using UnityEngine;

public class Pedal : MonoBehaviour {

	void Start () {
		
	}

    private void OnMouseDown()
    {
        Manager.isPedalPressed = true;
        transform.parent.Rotate((float)24.08401, 0, 0);
        var coroutine = Wait(0.3F);
        StartCoroutine(coroutine);
    }

    private IEnumerator Wait(float time)
    {
        yield return new WaitForSeconds(time);
        transform.parent.Rotate((float)-24.08401, 0, 0);
    }
}

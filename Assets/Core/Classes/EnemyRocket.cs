﻿using System.Collections;
using System.Collections.Generic;
using Assets.Core.Classes.Logic;
using UnityEngine;

public class EnemyRocket : MonoBehaviour {

    public int id;
    public RocketTypes type;
    public float speed;
    private bool isAttack = false;
    private bool isPushed = false;

    public GameObject target;


    void Start()
    {

    }

    void OnApplicationQuit()
    {
        if (!Assets.Core.Classes.Logic.Network.IsNetworkActive())
        {
            Assets.Core.Classes.Logic.Network.CloseSocket();
        }
    }


    void Update()
    {
        if (!isAttack)
        {
            return;
        }
        speed += 10;
        if (target == null)
        {
            if (!isPushed)
            {
                GetComponent<Rigidbody>().useGravity = true;

                GetComponent<Rigidbody>().AddForce(transform.forward * speed, ForceMode.Impulse);
                isPushed = true;
            }
            return;
        }
        if (Physics.Linecast(transform.position, target.transform.position))
        {
            Debug.DrawLine(transform.position, target.transform.position);
            target = null;
            return;
        }
        if (Vector3.Distance(gameObject.transform.position, target.transform.position) <= 10)
        {
            Gun[] guns = Resources.FindObjectsOfTypeAll<Gun>();
            guns[0].die();
            StartCoroutine(LoadLevelAfterDelay(0));
            die();
            return;
        }

        transform.position = Vector3.MoveTowards(
            transform.position,
            target.gameObject.transform.position,
            (float)Time.deltaTime * speed * 1.01F
        );
        Quaternion r = Quaternion.LookRotation(target.gameObject.transform.position - transform.position);

        transform.rotation = r;
    }

    private static IEnumerator LoadLevelAfterDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            Manager.endTask();
        }
    }

    public void attack()
    {
        isAttack = true;
    }

    public void die()
    {
        gameObject.transform.position = new Vector3(0, 0, 0);
    }
}

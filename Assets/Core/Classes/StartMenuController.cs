﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using Assets.Core.Classes.Logic;
using System;
using System.IO;

public class StartMenuController : MonoBehaviour
{
    private const string apache_label = "Apache";
    private const string f15_label = "F15";

    public volatile bool startonline;
    public volatile object dataonline;
    public Image panel;
    private Task task;
    public MenuLayer startLayer;
    public MenuLayer secondLayer;
    public MenuLayer waitingLayer;
    public MenuLayer endTaskLayer;
    public MenuLayer errorLayer;
    public MenuLayer pathConfigLayer;
    public MenuLayer pathConfigLayerkabr45;

    // First Layer
    public Button studyTestBtn;
    public Button trainingTestBtn;
    public Button onlineTestBtn;
    public InputField surnameStudent;
    public InputField nameStudent;
    public InputField squadStudent;

    // Second Layer
    public Button startTestBtn;
    public Button secondLayerBackBtn;
    public Button gotoPathConfigBtn;
    public Dropdown weatherSelect;
    public Dropdown mapSelect;
    public Dropdown targetType;
    public Dropdown targetPath;
    public Dropdown targetPathHelicopter;
    public Dropdown r1;
    public Dropdown r2;
    public Dropdown r3;
    public Dropdown r4;
    // Целеуказание
    public Dropdown targetNavigator;
    // Постановка помех
    public Dropdown noises;
    // Начальный азимут
    public InputField startAzimut;
    // Начальная дальность
    public InputField startFar;
    // Высота полета
    //public InputField startHeight;  

    // Waiting layer
    public Button cancelConnectionBtn;

    // End layer
    public Button okEndBtn;
    public Text resultMessageText;

    // Error layer
    public Button okErrorBtn;
    public Text msgError;

    // Path Config layer
    public Button applyBtn;
    public Button backPathConfigBtn;
    public InputField speed1field;
    public InputField speed2field;
    public InputField speed3field;
    public InputField speed4field;
    public InputField height1field;
    public InputField height2field;
    public InputField height3field;
    public InputField angleAfield;
    public InputField kursParamField;

    // Path Config - Kabr45 layer
    public Button applyKabrBtn;
    public Button backPathConfigKabrBtn;
    public InputField speed1Kabrfield;
    public InputField speed2Kabrfield;
    public InputField speed3Kabrfield;
    public InputField speed4Kabrfield;
    public InputField height1Kabrfield;
    public InputField kursParamKabrField;

    private int speed1;
    private int speed2;
    private int speed3;
    private int speed4;
    private int height1;
    private int height2;
    private int height3;

    void Awake()
    {
        if (secondLayer.gameObject.active)
        {
            startAzimut = GameObject.Find("StartAzimut").GetComponent<InputField>();
            startFar = GameObject.Find("StartFar").GetComponent<InputField>();
            //startHeight = GameObject.Find("StartHeight").GetComponent<InputField>();
        }
        if (pathConfigLayer.gameObject.active)
        {
            speed1field = GameObject.Find("Speed1").GetComponent<InputField>();
            speed2field = GameObject.Find("Speed2").GetComponent<InputField>();
            speed3field = GameObject.Find("Speed3").GetComponent<InputField>();
            speed4field = GameObject.Find("Speed4").GetComponent<InputField>();
            height1field = GameObject.Find("Height1").GetComponent<InputField>();
            height2field = GameObject.Find("Height2").GetComponent<InputField>();
            height3field = GameObject.Find("Height3").GetComponent<InputField>();
            angleAfield = GameObject.Find("UgolA").GetComponent<InputField>();
            kursParamField = GameObject.Find("KursParam").GetComponent<InputField>();
        }          
        if (pathConfigLayerkabr45.gameObject.active)
        {
            speed1Kabrfield = GameObject.Find("Speed1").GetComponent<InputField>();
            speed2Kabrfield = GameObject.Find("Speed2").GetComponent<InputField>();
            speed3Kabrfield = GameObject.Find("Speed3").GetComponent<InputField>();
            speed4Kabrfield = GameObject.Find("Speed4").GetComponent<InputField>();
            height1Kabrfield = GameObject.Find("Height1").GetComponent<InputField>();
            kursParamKabrField = GameObject.Find("KursParam").GetComponent<InputField>();
        }
        surnameStudent = GameObject.Find("SurnameInputField").GetComponent<InputField>();
        nameStudent = GameObject.Find("NameInputField").GetComponent<InputField>();
        squadStudent = GameObject.Find("SquadInputField").GetComponent<InputField>();
    }

    void Start()
    {
        studyTestBtn.onClick.AddListener(OpenStudyTest);
        trainingTestBtn.onClick.AddListener(OpenTrainingTest);
        onlineTestBtn.onClick.AddListener(OpenOnlineTest);
        startTestBtn.onClick.AddListener(StartTestOnClick);
        secondLayerBackBtn.onClick.AddListener(BackToStart);
        cancelConnectionBtn.onClick.AddListener(cancelConnectionOnClick);
        okEndBtn.onClick.AddListener(EndTaskAction);
        okErrorBtn.onClick.AddListener(ErrorHideAction);
        applyBtn.onClick.AddListener(ApplyPathConfig);
        backPathConfigBtn.onClick.AddListener(BackPathConfig);
        gotoPathConfigBtn.onClick.AddListener(GoToPathConfig);
        applyKabrBtn.onClick.AddListener(ApplyPathConfig);
        backPathConfigKabrBtn.onClick.AddListener(BackPathConfig);
        if (Manager.isEnd)
        {
            startLayer.hide();
            endTaskLayer.show();
            resultMessageText.text = Manager.endMessage;
        }
    }

    void trytoStart(object data)
    {
        //object data = dataonline;
        Task task = JsonConvert.DeserializeObject<Task>(data.ToString());  // Принимаем данные с сервера
        Manager.startTask(Task.GetCurrentTask());
    }

    void StartTestOnClick()                                                // OFFLINE TEST!!!!!!!!!!!!!!!!!
    {
        switch(targetNavigator.value) {
            case 0:
                Manager.CUType = "ЦУ от коммандира";
                break;
            case 1:
                Manager.CUType = "С АЦУ";
                break;
            case 2:
                Manager.CUType = "С ПРП";
                break;
        }
        
        int far;
        Int32.TryParse(startFar.text, out far);
        

        SaveUtil.taskParams = new Dictionary<string, string>();

        if (surnameStudent.text.Equals(""))
        {
            surnameStudent.text = "Неизвестный";
        }

        if (nameStudent.text.Equals(""))
        {
            nameStudent.text = "Не указано";
        }

        if (squadStudent.text.Equals(""))
        {
            squadStudent.text = "(Взвод не указан)";
        }
        User.GetInstance().name = surnameStudent.text + " " + nameStudent.text;
        User.GetInstance().squad = squadStudent.text;

        /*
            0 - безоблачно
            1 - легкая облачность
            2 - темное небо
            3 - туман
        */

        int weather = weatherSelect.value;
        int sky = weather;
        int map = mapSelect.value + 1;
        bool fog = false;
        int target = targetType.value;
        int targetTrajectory = targetPath.value;

        if (targetType.captionText.text.Equals(f15_label))
        {
            switch (targetTrajectory)
            {
                case 2:
                    targetTrajectory = 4; // с кабрирования 45
                    break;
            }

            startFar.placeholder.GetComponent<Text>().text = "8000-20000";

            if (far < 8000)
            {
                errorLayer.show();
                msgError.text = "Дальность не может быть меньше 8000 м";
                startFar.text = 8000.ToString();
                return;
            }
            if (far > 20000)
            {
                errorLayer.show();
                msgError.text = "Дальность не может быть больше 20000 м";
                startFar.text = 20000.ToString();
                return;
            }
        }
        
        if (targetType.captionText.text.Equals(apache_label))
        {
            switch (targetPathHelicopter.value)
            {
                case 0:
                    break;
                case 1: // с подскоком
                    targetTrajectory = 2;
                    break;
                case 2: // с пикирования 15
                    targetTrajectory = 3;
                    break;
            }

           
            startFar.placeholder.GetComponent<Text>().text = "6000-16000";

            if (far < 6000)
            {
                errorLayer.show();
                msgError.text = "Дальность не может быть меньше 6000 м";
                startFar.text = 6000.ToString();
                return;
            }
            if (far > 16000)
            {
                errorLayer.show();
                msgError.text = "Дальность не может быть больше 16000 м";
                startFar.text = 16000.ToString();
                return;
            }

        }
        SaveUtil.taskParams.Add("Фоновая обстановка", weatherSelect.captionText.text);
        SaveUtil.taskParams.Add("Местность", mapSelect.captionText.text);
        SaveUtil.taskParams.Add("Тип цели", targetType.captionText.text);
        SaveUtil.taskParams.Add("Траектория цели", targetPath.captionText.text);

        List<Enemy> ens = new List<Enemy>();
        Enemy t;


        switch (target)
        {
            // F15
            case 0:
                t = Resources.Load<Enemy>("EnemyF15");
                t.mapPath = targetTrajectory;
                ens.Add(t);
                break;
            // Apache
            case 1:
                t = Resources.Load<Enemy>("EnemyApache");
                t.mapPath = targetTrajectory;
                ens.Add(t);
                break;
            // BPLA
            case 2:
                t = Resources.Load<Enemy>("EnemyBPLA");
                t.mapPath = targetTrajectory;
                ens.Add(t);
                break;
            // A10
            case 3:
                t = Resources.Load<Enemy>("EnemyA10");
                t.mapPath = targetTrajectory;
                ens.Add(t);
                break;
            default:
                t = Resources.Load<Enemy>("EnemyF15");
                t.mapPath = targetTrajectory;
                ens.Add(t);
                break;
        }
        Manager.enemy = t;



        if (weather == 3)
        {
            fog = true;
            SaveUtil.taskParams.Add("Туман", "Присутствует");
            sky = 0;
        }

        List<RocketTypes> rockets = new List<RocketTypes>();

        switch (r1.value)
        {
            case 0:
                rockets.Add(RocketTypes.M37);
                break;
            case 1:
                rockets.Add(RocketTypes.M37M);
                break;
            case 2:
                rockets.Add(RocketTypes.M333);
                break;

        }

        switch (r2.value)
        {
            case 0:
                rockets.Add(RocketTypes.M37);
                break;
            case 1:
                rockets.Add(RocketTypes.M37M);
                break;
            case 2:
                rockets.Add(RocketTypes.M333);
                break;

        }

        switch (r3.value)
        {
            case 0:
                rockets.Add(RocketTypes.M37);
                break;
            case 1:
                rockets.Add(RocketTypes.M37M);
                break;
            case 2:
                rockets.Add(RocketTypes.M333);
                break;

        }

        switch (r4.value)
        {
            case 0:
                rockets.Add(RocketTypes.M37);
                break;
            case 1:
                rockets.Add(RocketTypes.M37M);
                break;
            case 2:
                rockets.Add(RocketTypes.M333);
                break;

        }

        task = new Task(sky, map, fog, ens, rockets);
        
        try
        {
            Manager.startTargetFar = Convert.ToInt32(startFar.text) * 2;
        }
        catch (Exception ignored) { }
        //try
        //{
        //    Manager.startAzimut = Convert.ToInt32(startAzimut.text);
        //}
        //catch (Exception ignored) { }


        // SAVING TO THE FILE 
        Manager.Result = new Dictionary<string, string>();
        Manager.Result.Add("Фоновая обстановка", weatherSelect.options[weather].text);
        Manager.Result.Add("Местность", mapSelect.options[map - 1].text);
        Manager.Result.Add("Тип цели", targetType.options[target].text);
        Manager.Result.Add("Ракета 1", r1.options[r1.value].text);
        Manager.Result.Add("Ракета 2", r2.options[r2.value].text);
        Manager.Result.Add("Ракета 3", r3.options[r3.value].text);
        Manager.Result.Add("Ракета 4", r4.options[r4.value].text);
        Manager.Result.Add("Начальная дальность", startFar.text);
        Manager.startTask(task);
        
    }

    void OnApplicationQuit()
    {
        if (!Assets.Core.Classes.Logic.Network.IsNetworkActive())
        {
            Assets.Core.Classes.Logic.Network.CloseSocket();
        }
    }

    void BackToStart()
    {
        Manager.isStudyMode = false;
        secondLayer.back();
    }

    void OpenStudyTest()
    {
        startLayer.hide();
        secondLayer.show();
        Manager.isStudyMode = true;
    }

    void OpenTrainingTest()
    {
        startLayer.hide();
        secondLayer.show();
    }

    void EndTaskAction()
    {
        endTaskLayer.hide();
        startLayer.show();
        Manager.isEnd = false;
        Manager.isStudyMode = false;
        Manager.stepNumber = 1;
    }

    void ErrorHideAction()
    {
        errorLayer.hide();
        secondLayer.show();
    }

    void ApplyPathConfig()
    {
        if (pathConfigLayer.isActiveAndEnabled)
        {
            pathConfigLayer.hide();
        }
        else if (pathConfigLayerkabr45.isActiveAndEnabled)
        {
            pathConfigLayerkabr45.hide();
        }
        secondLayer.show();
        int speed1, speed2, speed3, speed4, height1, height2, height3;
        // THINK ABOUT HOW TO ADD DEFAULT VALUES
        if (!int.TryParse(speed1field.text, out speed1)) speed1 = 150;
        if (!int.TryParse(speed2field.text, out speed2)) speed2 = 150;
        if (!int.TryParse(speed3field.text, out speed3)) speed3 = 150;
        if (!int.TryParse(speed4field.text, out speed4)) speed4 = 150;
        if (!int.TryParse(height1field.text, out height1)) height1 = 150;
        if (!int.TryParse(height2field.text, out height2)) height2 = 150;
        if (!int.TryParse(height3field.text, out height3)) height3 = 150;
        if (!int.TryParse(speed1Kabrfield.text, out speed1)) speed1 = 600;
        if (!int.TryParse(speed2Kabrfield.text, out speed2)) speed2 = 550;
        if (!int.TryParse(speed3Kabrfield.text, out speed3)) speed3 = 700;
        if (!int.TryParse(speed4Kabrfield.text, out speed4)) speed4 = 1200;
        if (!int.TryParse(height1Kabrfield.text, out height1)) height1 = 300;
        Manager.speedsList = new List<int>(){ 0, 0, 0, 0 };
        Manager.speedsList[0] = speed1;
        Manager.speedsList[1] = speed2;
        Manager.speedsList[2] = speed3;
        Manager.speedsList[3] = speed4;
        Manager.heightsList[0] = height1;
        Manager.heightsList[1] = height2;
        Manager.heightsList[2] = height3;
        int.TryParse(kursParamField.text, out Manager.kursParam);
        int.TryParse(kursParamKabrField.text, out Manager.kursParam);
        Manager.customPathValues = true;
    }

    void BackPathConfig()
    {
        if (pathConfigLayer.isActiveAndEnabled)
        {
            pathConfigLayer.hide();
        } else if (pathConfigLayerkabr45.isActiveAndEnabled)
        {
            pathConfigLayerkabr45.hide();
        }
        secondLayer.show();
    }
    
    void GoToPathConfig()
    {
        // Переход в нужный слой.
        switch (targetType.captionText.text)
        {
            case f15_label:
                switch (targetPath.value)
                {
                    case 2:
                        pathConfigLayerkabr45.show();
                        break;
                }
                break;
            case apache_label:
                switch (targetPathHelicopter.value)
                {
                    case 2:
                        pathConfigLayer.show();
                        break;
                }
                break;
        }
        
        secondLayer.hide();
    }

    void OpenOnlineTest()                                                                                                       // ONLINE TEST!!!!!!!!!!!
    {
        startLayer.hide();
        waitingLayer.show();
        Assets.Core.Classes.Logic.Network network = Assets.Core.Classes.Logic.Network.GetInstance();

        if (surnameStudent.text.Equals(""))
        {
            surnameStudent.text = "Неизвестный";
        }

        if (nameStudent.text.Equals(""))
        {
            nameStudent.text = "Не указано";
        }

        if (squadStudent.text.Equals(""))
        {
            squadStudent.text = "(Взвод не указан)";
        }

        network.Run(surnameStudent.text + " " + nameStudent.text, squadStudent.text);
        network.Socket.On("task", (data) =>
        {
            dataonline = data;
            startonline = true;
        });



        /*
			1. Подключаемся по Socket.IO
			2. Передаем сообщение, что мы студент
			3. Передаем наши данные (User)
			4. Ожидаем получение Task
			5. Manager.startTask(task)
		*/

    }

    void cancelConnectionOnClick()
    {
        Assets.Core.Classes.Logic.Network.CloseSocket();
        waitingLayer.back();
    }

    void Update()
    {
        bool configPathButtonTurnOn = false;
        switch (targetType.captionText.text)
        {
            case f15_label:
                switch (targetPath.value)
                {
                    case 2:
                        configPathButtonTurnOn = true;
                        break;
                    default:
                        configPathButtonTurnOn = false;
                        break;
                }
                break;
            case apache_label:
                switch (targetPathHelicopter.value)
                {
                    case 2:
                        configPathButtonTurnOn = true;
                        break;
                    default:
                        configPathButtonTurnOn = false;
                        break;
                }
                break;
            default:
                configPathButtonTurnOn = false;
                break;
        }
        gotoPathConfigBtn.gameObject.SetActive(configPathButtonTurnOn);

        if (targetType.captionText.text.Equals(apache_label))
        {
            targetPathHelicopter.gameObject.SetActive(true);
            targetPath.gameObject.SetActive(false);
        }
        else
        {
            targetPathHelicopter.gameObject.SetActive(false);
            targetPath.gameObject.SetActive(true);
        }

        if (!startAzimut.text.Equals(""))
        {
            if (startAzimut.text.Equals("-") || Int32.Parse(startAzimut.text) < 0)
            {
                startAzimut.text = 0.ToString();
            }
            else if (Int32.Parse(startAzimut.text) > 359)
            {
                startAzimut.text = 359.ToString();
            }
        }

        if (!targetType.captionText.text.Equals(apache_label))
        {
        
            int far;
            Int32.TryParse(startFar.text, out far);

            switch (targetPath.value)
            {
                case 0:
                    // неманеврирующая
                    startFar.placeholder.GetComponent<Text>().text = "8000-20000";
                    break;
                case 1:
                    // 
                    startFar.placeholder.GetComponent<Text>().text = "8000-20000";
                    break;
                case 2:
                    // кабрирование 45 град
                    startFar.placeholder.GetComponent<Text>().text = "8000-20000";
                    break;
                case 3:
                    // пик с простым маневром
                    startFar.placeholder.GetComponent<Text>().text = "12000-20000";
                    break;
                case 4:
                    // пик (угол 30 град)
                    startFar.placeholder.GetComponent<Text>().text = "8000-12000";
                    break;
            }

        }
        else
        {
            int far;
            Int32.TryParse(startFar.text, out far);
            switch (targetPath.value)
            {
                case 0:
                    // неманеврирующая
                    startFar.placeholder.GetComponent<Text>().text = "6000-16000";
                    break;
                case 1:
                    // зависание
                    startFar.placeholder.GetComponent<Text>().text = "6000-16000";
                    break;
                case 2:
                    // пикирование 15 град
                    startFar.placeholder.GetComponent<Text>().text = "6000-16000";
                    break;
            }

        }
        object data = dataonline;
        if (startonline)
        {
            trytoStart(dataonline);
        }


    }

    public string getSurname(string surname)
    {
        return surname;
    }

    public string getName(string name)
    {
        return name;
    }

    public string getSquad(string squad)
    {
        return squad;
    }

}

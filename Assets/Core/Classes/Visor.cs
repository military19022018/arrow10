﻿using UnityEngine;
using System.Collections;

public class Visor : MonoBehaviour
{
	public Camera cam;
    public Vector3 camCenter { get; private set; }

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
        camCenter = cam.ScreenToWorldPoint(new Vector3(Screen.width / 2f, Screen.height / 2f, cam.nearClipPlane));
	}

    void OnApplicationQuit()
    {
        if (!Assets.Core.Classes.Logic.Network.IsNetworkActive())
        {
            Assets.Core.Classes.Logic.Network.CloseSocket();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using Assets.Core.Classes.Logic;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveUtil {

    User user;

    public static Dictionary<string, string> taskParams { get; set; }
    private const string SEPARATOR_TEXT_FOR_SAVING =
            "-------------------------------------------------------------------------";
    public static Dictionary<String, String> BuildResult()
    {
        save(Manager.Result);
        return Manager.Result;
    }

    public static void GetResultsThumblers(Dictionary<string, string> tumblersStates)
    {
        foreach (var item in Manager.thumblers)
        {
            tumblersStates.Add(item.Name, GetThumblerStateString(item));
        }
    }

    private static string GetThumblerStateString(Thumbler thumbler)
    {
        if (thumbler is ThreeStatesThumbler)
        {
            return !((thumbler as ThreeStatesThumbler).State == ThreeStates.MIDDLE) ? "Верно" : "Не верно"; 
        }

        return (thumbler as TwoStatesThumbler).State == TwoStates.ON ? "Верно" : "Не верно";
    }
    
    public static void save(Dictionary<string, string> results)
    {
        int duplicateStudentsCount = 0;
        StringBuilder saveResultHelper = new StringBuilder();

        saveResultHelper.Append(SEPARATOR_TEXT_FOR_SAVING);
        saveResultHelper.AppendLine();
        saveResultHelper.Append("Тест: \"Боевая работа\"");
        saveResultHelper.AppendLine();
        saveResultHelper.Append(SEPARATOR_TEXT_FOR_SAVING);
        saveResultHelper.AppendLine();
        saveResultHelper.Append("Выполнил: " + User.GetInstance().name);
        saveResultHelper.AppendLine();
        saveResultHelper.Append("Взвод: " + User.GetInstance().squad);
        saveResultHelper.AppendLine();
        saveResultHelper.Append(SEPARATOR_TEXT_FOR_SAVING);
        saveResultHelper.AppendLine();

        foreach (var item in results)
        {
            saveResultHelper.Append(item.Key + ": " + item.Value);
            saveResultHelper.AppendLine();
        }


        //StreamWriter sw = new StreamWriter(Application.dataPath + Path.DirectorySeparatorChar + "lol" + ".txt");
        //sw.WriteLine("asdsadasdasd");
        //sw.Close();

        try
        {
            var fileDir = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                            Path.DirectorySeparatorChar + "Боевая работа - Результаты" +
                            Path.DirectorySeparatorChar + User.GetInstance().squad;
            var filename = fileDir + Path.DirectorySeparatorChar + User.GetInstance().name + ".doc";
            Directory.CreateDirectory(fileDir);
            File.WriteAllText(filename, saveResultHelper.ToString());
        } catch (Exception ex) {
            var errorFilepath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                      Path.DirectorySeparatorChar + "1.txt";
            File.WriteAllText(errorFilepath, ex.Message);
        }


        //string result = saveResultHelper.ToString();

        //string currentDate = DateTime.Now.ToString("dd.MM.yyyy");
        //string dirPath = "C:\\Users\\" + Environment.UserName +"\\Desktop\\Результаты теста 'Стрела-10 Боевая работа'\\"
        //    + currentDate + "\\" + User.GetInstance().squad;
        //string filePath = dirPath + "\\" + User.GetInstance().name + ".doc";

        //try
        //{
        //    if (!System.IO.File.Exists(filePath))
        //    {
        //        System.IO.File.WriteAllText(filePath, result);
        //    }
        //    else
        //    {
        //        duplicateStudentsCount++;
        //        filePath = dirPath + "\\" + User.GetInstance().name + "(" + duplicateStudentsCount + ").doc";
        //        System.IO.File.WriteAllText(filePath, result);
        //    }
        //}
        //catch (System.IO.DirectoryNotFoundException)
        //{
        //    System.IO.Directory.CreateDirectory(dirPath);
        //    System.IO.File.WriteAllText(filePath, result);
        //}  

    }
}

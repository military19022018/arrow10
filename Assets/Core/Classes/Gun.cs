﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Core.Classes.Logic;
using UnityEngine.UI;
using System.Linq;
using System;

public class Gun : MonoBehaviour
{
    public ParticleSystem explosion;

    private bool gashetkaActive = false;

    private const string tumb_24v = "Тумблер 24В";
    private const string tumb_28v = "Тумблер 28В";
    private const string tumb_aoz = "Тумблер АОЗ";
    private const string tumb_privod = "Тумблер Привод";
    private const string tumb_fon = "Тумблер Фон";
    private const string tumb_rodraboti = "rodraboti";
    private const string tumb_perevod = "Тумблер Перевод";

    public static Quaternion angle;

    private bool cancelFirstStart = false;
    public GameObject SpawnR1;
    public GameObject SpawnR2;
    public GameObject SpawnR3;
    public GameObject SpawnR4;
    public GameObject Mark;
    public Rocket R1;
    public Rocket R2;
    public Rocket R3;
    public Rocket R4;
    public float xx, yy, zz;
    public Camera visorCamera;
    public GameObject zonaON;
    public GameObject glassScope;
    public GameObject vizirScope;
    public Text step1;
    public Text step2;
    public Text step3;
    public Text step4;
    public Text step5;
    public Text step5_5;
    public Text step6;
    public Text step7;
    public Text step8;

    public GameObject strelkaVniz;

    public GameObject pravoON;
    public GameObject levoON;

    public GameObject green1;
    public GameObject green2;
    public GameObject green3;
    public GameObject red1;
    public GameObject red2;

    GunAttackController gac;

    public static bool bortActivated = false;
    private bool shootingAccess = true;
    private bool slejenieActive = false;
    private float timer = 5;
    private static float maxHeight = -42.0f;

    public static int targetDistance;
    public static int targetXDistance;
    public static int targetYDistance;
    public static int targetZDistance;

    public Vector3 vectorToTarget;

    public ParticleSystem rocketSmoke;

    //private IEnumerator coroutine;

    float camSpeed = 15;
    float slowCamSpeed = 50;
    public static float strelkaRotation = 0;

    float currentCamSpeed;

    public Text targetFarLABEL;
    public Text targetHeightLABEL;
    public Text targetAngleLABEL;
    public Text targetFarLABEL_Glass;
    public Text targetHeightLABEL_Glass;
    public Text targetAngleLABEL_Glass;
    public Text UpscaleValue;
    public Text ACUStatus;

    public float ugolVrasheniaPU;
    private bool acuEnabled = false;


    void Start()
    {
        angle = transform.rotation;
        //GameObject strelka = GameObject.Find("UA-SEKTOR");
        //strelka.transform.rotation = Quaternion.Euler(0, 0, 0);
        strelkaRotation = 0;
        explosion = Instantiate(Resources.Load<ParticleSystem>("Explosion"));
    }


    /**
     * СИГНАЛ ПРАВО
     */
    void rightSector()
    {
        Enemy[] enemies = Resources.FindObjectsOfTypeAll<Enemy>();
        Vector3 vector = Camera.main.WorldToScreenPoint(enemies[0].transform.position);
        xx = vector.x;
        Vector3 relativePos = enemies[0].transform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos);
        Vector3 curRot = transform.rotation.eulerAngles;
        var ugol = rotation.eulerAngles.y - curRot.y;

        if (ugol > 7)
        {
            pravoON.SetActive(true);
        }
        else 
        { 
            pravoON.SetActive(false); 
        }
    }

    /**
    * СИГНАЛ ВЛЕВО
    */
    void leftSector()
    {
        Enemy[] enemies = Resources.FindObjectsOfTypeAll<Enemy>();
        Vector3 vector = Camera.main.WorldToScreenPoint(enemies[0].transform.position);
        xx = vector.x;
        Vector3 relativePos = enemies[0].transform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos);
        Vector3 curRot = transform.rotation.eulerAngles;
        var ugol = rotation.eulerAngles.y - curRot.y;
        
        if (ugol < -7)
        {

            levoON.SetActive(true);
        }
        else 
        { 
            levoON.SetActive(false); 
        }
    }

    void distanceUpdate() 
    {
        Enemy[] enemies = Resources.FindObjectsOfTypeAll<Enemy>();
        float distance = Vector3.Distance(enemies[0].transform.position, transform.position);
        targetDistance = (int)Mathf.Abs(distance);
        Vector3 enemyPos = enemies[0].transform.position;
        Vector3 ourPos = transform.position;
        targetXDistance = (int)Mathf.Abs(enemyPos.x - ourPos.x);
        targetYDistance = (int)Mathf.Abs(enemyPos.y - ourPos.y);
        targetZDistance = (int)Mathf.Abs(enemyPos.z - ourPos.z);
    }

    /*
     * ЗОНА
     */
    bool checkAvaliableDistance() 
    {
        if (targetDistance > 5000 || (targetYDistance) > 3500)
        {
            return false;
        }
        return true;
    }

    bool checkKolcoPerecresteVision()
    {
        Enemy[] enemies = Resources.FindObjectsOfTypeAll<Enemy>();
        Vector3 vector = Camera.main.WorldToScreenPoint(enemies[0].transform.position);
        xx = vector.x;
        yy = vector.y;
        float distance = Mathf.Sqrt((Screen.width / 2 - xx) * (Screen.width / 2 - xx) + 
                                    (Screen.height / 2 - yy) * (Screen.height / 2 - yy));
        if (distance > 50) {
            return false;
        }
        return true;
    }

    public void switchToVizir()
    {
        Manager.glassState = false;
        Manager.visorState = true;
    }

    void moveLeftByACU()
    {
        Enemy[] enemies = Resources.FindObjectsOfTypeAll<Enemy>();
        Vector3 vector = Camera.main.WorldToScreenPoint(enemies[0].transform.position);
        xx = vector.x;
        Vector3 relativePos = enemies[0].transform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos);
        Vector3 curRot = transform.rotation.eulerAngles;
        var ugol = rotation.eulerAngles.y - curRot.y;

        if (ugol < -7)
        {
            strelkaRotation += Time.deltaTime * currentCamSpeed;
            transform.RotateAround(visorCamera.transform.position, Vector3.up, -currentCamSpeed * Time.deltaTime);
        }
    }

    void moveRightByACU()
    {
        Enemy[] enemies = Resources.FindObjectsOfTypeAll<Enemy>();
        Vector3 vector = Camera.main.WorldToScreenPoint(enemies[0].transform.position);
        xx = vector.x;
        Vector3 relativePos = enemies[0].transform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos);
        Vector3 curRot = transform.rotation.eulerAngles;
        var ugol = rotation.eulerAngles.y - curRot.y;

        if (ugol > 7)
        {
            strelkaRotation += Time.deltaTime * -currentCamSpeed;
            transform.RotateAround(visorCamera.transform.position, Vector3.up, currentCamSpeed * Time.deltaTime);
        }
    }

    void Update()
    {

        var v24 = (TwoStatesThumbler)Manager.thumblers.Find(t => t.Name.Equals(tumb_24v));
        var v28 = (TwoStatesThumbler)Manager.thumblers.Find(t => t.Name.Equals(tumb_28v));
        var aoz = (TwoStatesThumbler)Manager.thumblers.Find(t => t.Name.Equals(tumb_aoz));
        var privod = (ThreeStatesThumbler)Manager.thumblers.Find(t => t.Name.Equals(tumb_privod));
        var fon = (ThreeStatesThumbler)Manager.thumblers.Find(t => t.Name.Equals(tumb_fon));
        var rodraboti = (FiveStatesThumbler)Manager.thumblers.Find(t => t.Name.Equals(tumb_rodraboti));
        var perevod = (TwoStatesThumbler)Manager.thumblers.Find(t => t.Name.Equals(tumb_perevod));
        //обрабатывает нажатие в окне
        if (visorCamera.gameObject.active && Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = visorCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.transform != null)
                {
                    string clickedObjectName = hit.transform.gameObject.name;
                    Debug.Log(clickedObjectName);
                    if (clickedObjectName.Equals("vizorPreview"))
                    {
                        switchToVizir();
                    }
                    else if (clickedObjectName.Equals("acuPreview"))
                    {
                        if (!acuEnabled)
                        {
                            acuEnabled = true;
                            ACUStatus.text = "АЦУ: \nудержание";
                        }
                        else
                        {
                            acuEnabled = false;
                            ACUStatus.text = "АЦУ: \nне нажата";
                        }
                    }
                }
            }
        }
        /*
         *Производит поворот ПУ согласно АЦУ
         */
        if (acuEnabled &&
           Input.GetKey(KeyCode.Space) &&
           Manager.CUType.Equals("С АЦУ") &&
           v24.State == TwoStates.ON &&
           v28.State == TwoStates.ON &&
            privod.State != ThreeStates.MIDDLE &&
            perevod.State == TwoStates.ON &&
            Manager.isPedalPressed)
        {
            moveLeftByACU();
            moveRightByACU();
        }

        Enemy[] enemies = Resources.FindObjectsOfTypeAll<Enemy>();


        if (Manager.glassState)
        {
            visorCamera.fieldOfView = 60;
            glassScope.SetActive(true);
            vizirScope.SetActive(false);
            visorCamera.farClipPlane = 5600;
        }
        else
        {
            if (visorCamera.fieldOfView != 16)
            {
                visorCamera.farClipPlane = 6200;
                visorCamera.fieldOfView = 22;
            }
            glassScope.SetActive(false);
            vizirScope.SetActive(true);
        }

        Vector3 xToTarget = new Vector3(targetXDistance, 0, targetZDistance);
        Vector3 toTarget = new Vector3(targetXDistance, targetYDistance, targetZDistance);

        float angle = (float) Math.Round((double)Vector3.Angle(toTarget, xToTarget),2);


        //Vector3 xPU = new Vector3(transform.position.x, 0, transform.position.z);
        //Debug.Log("XPU: " + xPU);
        //Debug.Log("transform rotation: " + transform.rotation.eulerAngles);
        //float angleOfPU = (float)Math.Round((double)Vector3.Angle(transform.rotation.eulerAngles, xPU));

        float puAngle = 180 - transform.rotation.eulerAngles.x % 180;
        Debug.Log("УГОЛ ПУ: " + puAngle);

        green1.SetActive(false);
        green2.SetActive(false);
        green3.SetActive(false);
        red1.SetActive(false);
        red2.SetActive(false);
        /*
         * 9Ш23М - ПОДСВЕЧИВАНИЕ УГЛА НАКЛОНА ПУ
         */
        if (puAngle >= 0 && puAngle < 7.5) {
            green1.SetActive(true);
        }
        if (puAngle >= 7.5 && puAngle < 15)
        {
            green2.SetActive(true);
        } 
        if (puAngle >= 15 && puAngle < 22.5)
        {
            red1.SetActive(true);
        }
        if (puAngle >= 22.5 && puAngle < 30)
        {
            red2.SetActive(true);
        }
        if (puAngle >= 30 && puAngle <= 37.5)
        {
            green3.SetActive(true);
        }



        distanceUpdate();
        /*
         * ОТОБРАЖЕНИЕ ДАННЫХ ПО ЦЕЛИ - ВИЗИР
         */
        targetFarLABEL.text = (targetDistance).ToString();
        targetHeightLABEL.text = (targetYDistance).ToString();
        targetAngleLABEL.text = angle.ToString();
        /*
         * ОТОБРАЖЕНИЕ ДАННЫХ ПО ЦЕЛИ - СТЕКЛО
         */
        targetFarLABEL_Glass.text = (targetDistance).ToString();
        targetHeightLABEL_Glass.text = (targetYDistance).ToString();
        targetAngleLABEL_Glass.text = angle.ToString();

        /*
         * режим ОБУЧЕНИЕ 
         */
        if (Manager.isStudyMode)
        {

            if(Manager.stepNumber == 6)
            {
                if (v24.State == TwoStates.ON &&
                    v28.State == TwoStates.ON &&
                    privod.State == ThreeStates.TOP &&
                    aoz.State == TwoStates.ON &&
                    perevod.State == TwoStates.ON &&
                    Manager.isPedalPressed == true &&
                    !levoON.active && 
                    !pravoON.active)
                {
                    Manager.stepNumber = 7;
                }
            }

            if (Manager.stepNumber == 7)
            {
                if (v24.State == TwoStates.ON &&
                    v28.State == TwoStates.ON &&
                    privod.State == ThreeStates.TOP &&
                    aoz.State == TwoStates.ON &&
                    perevod.State == TwoStates.ON &&
                Manager.isPedalPressed == true &&
                    !levoON.active &&
                    !pravoON.active &&
                    bortActivated
                   )
                {
                    Manager.stepNumber = 8;
                }
            }

            if (Manager.stepNumber == 8) 
            {
                if (v24.State == TwoStates.ON &&
                    v28.State == TwoStates.ON &&
                    privod.State == ThreeStates.TOP &&
                    aoz.State == TwoStates.ON &&
                    perevod.State == TwoStates.ON &&
                Manager.isPedalPressed == true &&
                    !levoON.active &&
                    !pravoON.active &&
                    bortActivated &&
                    slejenieActive
                   )
                {
                    Manager.stepNumber = 9;
                }
            }

            step1.gameObject.SetActive(false);
            step2.gameObject.SetActive(false);
            step3.gameObject.SetActive(false);
            step4.gameObject.SetActive(false);
            step5.gameObject.SetActive(false);
            step5_5.gameObject.SetActive(false);
            step6.gameObject.SetActive(false);
            step7.gameObject.SetActive(false);
            step8.gameObject.SetActive(false);

            switch (Manager.stepNumber)
            {
                case 1:
                    step1.gameObject.SetActive(true);
                    break;
                case 2:
                    step2.gameObject.SetActive(true);
                    break;
                case 3:
                    step3.gameObject.SetActive(true);
                    break;
                case 4:
                    step4.gameObject.SetActive(true);
                    break;
                case 5:
                    step5.gameObject.SetActive(true);
                    break;
                case 6:
                    step5_5.gameObject.SetActive(true);
                    break;
                case 7:
                    step6.gameObject.SetActive(true);
                    break;
                case 8:
                    step7.gameObject.SetActive(true);
                    break;
                case 9:
                    step8.gameObject.SetActive(true);
                    break;
                default:
                    break;
            }
        }

        /*
         * АОЗ - СТРЕЛКИ ПРАВО И ЛЕВО
         */
        if (v24.State == TwoStates.ON &&
            v28.State == TwoStates.ON &&
            Manager.CUType.Equals("С ПРП"))
        {
            rightSector();
            leftSector();
        }

        if (!shootingAccess)                                                                                       // TIMER IS WORK
        {
            timer -= Time.deltaTime;
        }



        /*
         * СЛЕЖЕНИЕ - УДЕРЖАНИЕ ПКМ
         */
        if (enemies.Length != 0 && Manager.enemyVisible && Input.GetKey(KeyCode.Mouse1) && 
            checkKolcoPerecresteVision() && bortActivated) // bort  
        {
            FindObjectOfType<move>().enabled = false;
            slejenieActive = true;
            if (checkAvaliableDistance())
            {
                zonaON.SetActive(true);
            } else {
                zonaON.SetActive(false);
            }
            Vector3 vector = Camera.main.WorldToScreenPoint(enemies[0].transform.position);
            xx = vector.x;
            yy = vector.y;
            Vector3 v = new Vector3(vector.x - Screen.width / 2, vector.y - Screen.height / 2, 0);
            Mark.transform.localPosition = v;
        }

        /*
         * СЛЕЖЕНИЕ - ЦЕЛЬ ВЫШЛА ИЗ КОЛЬЦА-ПЕРЕКРЕСТИЯ
         */
        if (enemies.Length != 0 && 
            Manager.enemyVisible && 
            Input.GetKey(KeyCode.Mouse1) &&
            !checkKolcoPerecresteVision() && 
            bortActivated)
        {
            FindObjectOfType<move>().enabled = true;
            slejenieActive = false;
            zonaON.SetActive(false);
            StartCoroutine(moveToCenterCoroutine());
        }

        /*
         * СЛЕЖЕНИЕ - ОТПУСКАЕМ ПКМ
         */
        if (Input.GetKeyUp(KeyCode.Mouse1) &&
            bortActivated)
        {
            FindObjectOfType<move>().enabled = true;
            slejenieActive = false;
            zonaON.SetActive(false);
            StartCoroutine(moveToCenterCoroutine());
        }

        /*
         * БОРТ
         */
        if (Manager.visorState &&
            Input.GetKeyDown(KeyCode.Q) && 
            v24.State == TwoStates.ON && 
            v28.State == TwoStates.ON && 
            privod.State != ThreeStates.MIDDLE && 
            perevod.State == TwoStates.ON) // 24, 28, privod, boevoe
        {
            this.StartCoroutine(moveToCenterCoroutine());
            bortActivated = true;
        }

        /*
         * ГАШЕТКА
         */
        if (Input.GetKey(KeyCode.Space) && 
            v24.State == TwoStates.ON && 
            v28.State == TwoStates.ON &&
            privod.State != ThreeStates.MIDDLE) // 24, 28 , privod
        {
            this.gashetkaActive = true;
            Debug.Log("gashetka");
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            this.gashetkaActive = false;
        }



        /*
         * КРАТНОСТЬ ВИЗИРА
         */
        if (Manager.visorState && 
            Input.GetKeyDown(KeyCode.F)) {
            if (UpscaleValue.text.Equals("x2.7"))
            {
                UpscaleValue.text = "x3.7";
                visorCamera.fieldOfView = 16;
                visorCamera.farClipPlane = 10700;
            } else {
                UpscaleValue.text = "x2.7";
                visorCamera.fieldOfView = 22;
                visorCamera.farClipPlane = 6200;
            }
        }
       

        /*
         * ВЫХОД С ВИЗИРА
         */
        if (Input.GetKey(KeyCode.Escape))
        {
            Manager.visorState = false;
            Manager.glassState = false;
            gameObject.SetActive(false);
            Operator[] ops = Resources.FindObjectsOfTypeAll<Operator>();
            ops[0].gameObject.SetActive(true);
        }

        currentCamSpeed = camSpeed;

        /*
         * УСКОРЕНИЕ ДВИЖЕНИЯ БАШНИ
         */
        if (Input.GetKey(KeyCode.LeftShift))
        {
            currentCamSpeed = slowCamSpeed;
        }



        float currentRot = transform.rotation.eulerAngles.x % 180;

        /*
         * СИГНАЛ ВНИЗ
         */
        if (currentRot > 0 && currentRot < 25)
        {
            strelkaVniz.SetActive(true);
        }
        else
        {
            strelkaVniz.SetActive(false);
        }


        /*
         * ДВИЖЕНИЕ ВВЕРХ/ВНИЗ
         */
        if (!Manager.glassState && gashetkaActive && Input.GetKey(KeyCode.W) && !(currentRot > 79 && currentRot < 99))
        {
            transform.Rotate(-currentCamSpeed * Time.deltaTime, 0, 0); //Крутим по оси Y
        }
        else if (!Manager.glassState && gashetkaActive && Input.GetKey(KeyCode.S) && !(currentRot > 5 && currentRot < 25))
        {
            transform.Rotate(currentCamSpeed * Time.deltaTime, 0, 0); //Крутим по оси Y в обратную сторону
        }


        /*
         * ПОВОРОТ СТРЕЛКИ НА СТЕКЛЕ
         */
        GameObject strelka = GameObject.Find("UA-SEKTOR");
        if (Manager.glassState)
        {
            strelka.transform.rotation = Quaternion.Euler(strelka.transform.rotation.eulerAngles.x,
                                                          strelka.transform.rotation.eulerAngles.y, 
                                                          strelkaRotation);
        }

        /*
         * ДВИЖЕНИЕ ВЛЕВО/ВПРАВО
         */
        if (!acuEnabled && gashetkaActive && Input.GetKey(KeyCode.A))
        {
            strelkaRotation += Time.deltaTime * currentCamSpeed;
            transform.RotateAround(visorCamera.transform.position, Vector3.up, -currentCamSpeed * Time.deltaTime);
        }
        else if (!acuEnabled && gashetkaActive && Input.GetKey(KeyCode.D))
        {
            strelkaRotation += Time.deltaTime * -currentCamSpeed;
            transform.RotateAround(visorCamera.transform.position, Vector3.up, currentCamSpeed * Time.deltaTime);
        }

        /*
         * СТРЕЛЬБА - ЛКМ
         */
        if (Input.GetKey(KeyCode.Mouse1) && Input.GetMouseButtonDown(0) && Manager.enemyVisible && checkAvaliableDistance())
        {
            if (App.State.Current_rocket == -1)
            {
                return;
            }

            RocketTypes rt = Manager.task.Rockets[App.State.Current_rocket];
            Rocket rTmp = null;
            switch (rt)
            {
                case RocketTypes.M333:
                    rTmp = Instantiate(Resources.Load<Rocket>("Rocket-9M333"));
                    break;
                case RocketTypes.M37:
                    rTmp = Instantiate(Resources.Load<Rocket>("Rocket-9M37"));
                    break;
                case RocketTypes.M37M:
                    rTmp = Instantiate(Resources.Load<Rocket>("Rocket-9M37M"));
                    break;
            }
            if (Manager.shootingAccess())
            {
                if (this.shootingAccess)
                {
                    switch (App.State.Current_rocket)
                    {
                        case 0:
                            R1 = Instantiate(rTmp, SpawnR1.transform.position, SpawnR1.transform.rotation);
                            R1.target = Manager.task.EnemyList[0];
                            R1.attack();
                            App.State.R1 = RocketTypes.NONE;
                            break;
                        case 1:
                            R2 = Instantiate(rTmp, SpawnR2.transform.position, SpawnR2.transform.rotation);
                            R2.target = Manager.task.EnemyList[0];
                            R2.attack();
                            App.State.R2 = RocketTypes.NONE;
                            break;
                        case 2:
                            R3 = Instantiate(rTmp, SpawnR3.transform.position, SpawnR3.transform.rotation);
                            R3.target = Manager.task.EnemyList[0];
                            R3.attack();
                            App.State.R3 = RocketTypes.NONE;
                            break;
                        case 3:
                            R4 = Instantiate(rTmp, SpawnR4.transform.position, SpawnR4.transform.rotation);
                            R4.target = Manager.task.EnemyList[0];
                            R4.attack();
                            App.State.R4 = RocketTypes.NONE;
                            break;
                    }
                }
                rocketSmoke.enableEmission = true;
                this.shootingAccess = false;
            }
        }
        if (timer <= 0)                                                                                      // TIMER IS CHECK 
        {
            rocketSmoke.enableEmission = false;
            this.shootingAccess = true;
            timer = 4;
        }

    }

    void OnApplicationQuit()
    {
        if (!Assets.Core.Classes.Logic.Network.IsNetworkActive())
        {
            Assets.Core.Classes.Logic.Network.CloseSocket();
        }
    }

    private IEnumerator SmokeWait(float waitTime)
    {
        while (true)
        {
            yield return new WaitForSeconds(waitTime);
            rocketSmoke.enableEmission = false;
            this.shootingAccess = true;
        }
    }

    // Fire rocket
    void Attack()
    {
        gac.Fire();
    }


    private IEnumerator moveToCenterCoroutine()
    {
        Camera vizor = Manager.visor.visorCamera;
        float breakDistance;
        while (true)
        {
            FindObjectOfType<move>().enabled = false;
            var movePosition = Vector3.MoveTowards(
                            Mark.transform.position,
                            vizor.ScreenToWorldPoint(new Vector3(Screen.width / 2f,
                                                     Screen.height / 2f,
                                                     (float)(vizor.nearClipPlane + 0.65))),
                            (float)(Time.deltaTime * 0.2));
            //TODO replace time.delta to constant time

            breakDistance = Vector3.Distance(movePosition, vizor.transform.position);
            if (breakDistance <= 0.95001) // Центр
            {
                Manager.BortPressed = true;
                FindObjectOfType<move>().enabled = true;
                break;
            }

            Mark.transform.position = movePosition;
            yield return new WaitForSeconds(0.01f);
        }

        this.StopCoroutine(moveToCenterCoroutine());
    }


    public void die()
    {
        //TODO send to Manager
        Instantiate(explosion, gameObject.transform.position, Quaternion.identity);

        SaveUtil.GetResultsThumblers(Manager.Result);



        if (!Manager.isStudyMode)
        {
            Manager.Result.Add("Тестирование: ", "НЕ ПРОЙДЕЕНО");
            Manager.endMessage = "Вы провалили задание на тренировке!";
            SaveUtil.BuildResult();
        }
        else
        {
            Manager.Result.Add("ОБУЧЕНИЕ: ", "НЕ ЗАВЕРШЕНО");
            Manager.endMessage = "Вы провалили обучение!";
        }
        Destroy(gameObject);
    }
}

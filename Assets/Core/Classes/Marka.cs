﻿using System.Collections;
using UnityEngine;
using Assets.Core.Classes.Logic;

public class Marka : MonoBehaviour {
    
    public GameObject marka;
    private Vector3 currentPos;
    public bool isWalk = true;
    private Renderer renderer;


	// Use this for initialization
	void Start () 
    {
        renderer = Manager.enemy.GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () 
    {

        currentPos = marka.transform.position;
        if (Input.GetKeyDown(KeyCode.Q))
        {
            this.StartCoroutine(moveToCenterCoroutine());
        }

        Manager.Marka = this;
	}

    void OnApplicationQuit()
    {
        if (!Assets.Core.Classes.Logic.Network.IsNetworkActive())
        {
            Assets.Core.Classes.Logic.Network.CloseSocket();
        }
    }

    public void move(Vector3 pos)
    {
        marka.transform.position = pos;
    }

    public void moveRightTop(Vector3 movePosition)
    {
        Camera vizor = Manager.visor.visorCamera;
        marka.transform.position = movePosition;
    }

    private IEnumerator moveToRightTopCoroutine()
    {
        Camera vizor = Manager.visor.visorCamera;
        float breakDistance;
        while (true)
        {
            var movePosition = Vector3.MoveTowards(
                        marka.transform.position,
                            vizor.ScreenToWorldPoint(new Vector3(Screen.width / 2f + 30,
                                                 Screen.height / 2f + 30,
                                                 (float)(vizor.nearClipPlane + 0.65))),
            (float)(Time.deltaTime * 0.15));

            breakDistance = Vector3.Distance(movePosition, vizor.transform.position);
            //TODO : Придумать адекватный break
            if (breakDistance >= 0.956415)
            {
                break;
            }
            moveRightTop(movePosition);
            yield return new WaitForSeconds(0.01f);
        }
        this.StopCoroutine(moveToRightTopCoroutine());
    }

    private IEnumerator moveToCenterCoroutine()
    {
        Camera vizor = Manager.visor.visorCamera;
        float breakDistance;
        while (true)
        {
            var movePosition = Vector3.MoveTowards(
                            marka.transform.position,
                            vizor.ScreenToWorldPoint(new Vector3(Screen.width / 2f,
                                                     Screen.height / 2f,
                                                     (float)(vizor.nearClipPlane + 0.65))),
                            (float)(Time.deltaTime * 0.2));
            //TODO replace time.delta to constant time

            breakDistance = Vector3.Distance(movePosition, vizor.transform.position);
            if (breakDistance <= 0.95001) // Центр
            //if (breakDistance <= 0.9583987) // Смещенное, для дальнейшей восьмерки
            {
                Manager.BortPressed = true;
                break;
            }

            move(movePosition);
            yield return new WaitForSeconds(0.01f);
        }

        this.StopCoroutine(moveToCenterCoroutine());
        // Запускает корутину чтобы как по восьмерке(вправо вверх)
        // Работает, но нужно break придумать
        //this.StartCoroutine(moveToRightTopCoroutine());
    }
}

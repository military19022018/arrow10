﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EPPoint : MonoBehaviour
{
    public float h;
    public float v;
    public double delay;
    public bool nonRotate;
    public float speedToPoint = 150;
    public bool attackEnemy;

    public void SetHeight(float height)
    {
        transform.position = new Vector3(transform.position.x, height, transform.position.z);
    }

    public void SetSpeedToPoint(float speed)
    {
        speedToPoint = speed;
    }

    void Start() { }
    void Update() {
        
    }

    public Vector3 position
    {
        get
        {
            return transform.position;
        }
    }

    public Quaternion rotation
    {
        get
        {
            return transform.rotation;
        }
    }

}

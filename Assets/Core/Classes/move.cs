﻿using UnityEngine;

public class move : MonoBehaviour {

    public float speed = 1;
    public float scaleX = 20;
    public float scaleY = 20;
    float cornerAngle = 0;

    void Update()
    {
        cornerAngle += Time.deltaTime * speed;
        Vector3 vector3 = Camera.main.ScreenToWorldPoint(new Vector3((Mathf.Cos(cornerAngle) * scaleX) + Screen.width / 2, 
                                                                     (Mathf.Sin(cornerAngle) * Mathf.Cos(cornerAngle) * scaleY) + Screen.height / 2, 
                                                                     1F));
        transform.position = vector3;
    }
}

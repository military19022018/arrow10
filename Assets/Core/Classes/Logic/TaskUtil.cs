﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Core.Classes.Logic
{
    internal sealed class TaskUtil
    {
        private TaskUtil()
        { }

        internal static Enemy CreateEnemy(string enemyType)
        {
            Enemy enemy = null;
            switch (enemyType)
            {
                case "Тип A10A":
                    enemy = Resources.Load<Enemy>("EnemyA10");
                    break;
                case "Вертолет":
                    enemy = Resources.Load<Enemy>("EnemyApache");
                    break;
                case "БПЛА":
                    enemy = Resources.Load<Enemy>("EnemyBPLA");
                    break;
                case "F15":
                    enemy = Resources.Load<Enemy>("EnemyF15");
                    break;
            }

            return enemy;
        }

        internal static int CreateEnemyTrajectory(string traj)
        {
            switch (traj)
            {
                case "Простая":
                    return 0;
                case "Усложненная":
                    return 1;
                case "Маневрирующая":
                    return 2;
            }

            throw new ArgumentException("Wrong enemy trajectory");
        }

        internal static int CreateScene(string map)
        {
            switch (map)
            {
                case "Земляная":
                    return 1;
                case "Пустыня":
                    return 2;
                case "Снежная":
                    return 3;
            }

            throw new ArgumentException("Wrong scene");
        }

        internal static int CreateWeather(string weather)
        {
            switch(weather)
            {
                case "Безоблачно":
                    return 0;
                case "Легкая облачность":
                    return 1;
                case "Темное небо":
                    return 2;
                case "Туман":
                    return 3;
            }

            throw new ArgumentException("Wrong weather type");
        }

        internal static List<RocketTypes> CreateRockets(string rocket1, string rocket2, string rocket3, string rocket4)
        {
            List<RocketTypes> rockets = new List<RocketTypes>();
            rockets.Add(CreateRocket(rocket1));
            rockets.Add(CreateRocket(rocket2));
            rockets.Add(CreateRocket(rocket3));
            rockets.Add(CreateRocket(rocket4));

            return rockets;
        }

        internal static RocketTypes CreateRocket(string rocketType)
        {
            switch(rocketType)
            {
                case "9М37":
                    return RocketTypes.M37;
                case "9М37М":
                    return RocketTypes.M37M;
                case "9М333":
                    return RocketTypes.M333;
            }

            throw new ArgumentException("Wrong rocket type");
        }

        internal static bool CreateForg(int weather)
        {
            return weather == 3;
        }
    }
}

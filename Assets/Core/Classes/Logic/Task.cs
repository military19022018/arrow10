﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
//using UnityEditor;
using Newtonsoft.Json;

namespace Assets.Core.Classes.Logic
{
    [Serializable]
    public class Task
    {
        [NonSerialized]
        private static Task instance;

        [NonSerialized]
        private int scene;
        public int Scene
        {
            get { return scene; }
            set { scene = value; }
        }

        [NonSerialized]
        private List<Enemy> enemyList;
        public List<Enemy> EnemyList
        {
            get { return enemyList; }
            set { enemyList = value; }
        }

        [NonSerialized]
        private int skybox;
        public int Skybox
        {
            get { return skybox; }
            set { skybox = value; }
        }

        [NonSerialized]
        private Boolean fog;
        public Boolean Fog
        {
            get { return fog; }
            set { fog = value; }
        }

        [NonSerialized]
        private List<RocketTypes> rockets;
        public List<RocketTypes> Rockets
        {
            get { return rockets; }
            set { rockets = value; }
        }

        //Тип ЛА
        public string laType { get; set; }

        //Скорость ЛА
        public string laSpeed { get; set; }

        //Азимут(0-359), распарсить в int можно
        public string azimuth { get; set; }

        //Целеуказание
        public string aimData { get; set; }

        //Погодные условия
        public string weather { get; set; }

        //Тип помех
        public string noisesType { get; set; }

        //Может быть null, обязательно проверять на null
        public string noisesTime { get; set; }

        //Тип траектории
        public string tragectory { get; set; }

        //Тип карты
        public string map { get; set; }

        //Начальная дальность цели
        public string startRange { get; set; }

        //Высота цели
        public string startHeight { get; set; }

        //Тип ракеты 1
        public string projectile1 { get; set; }

        //Тип ракеты 2
        public string projectile2 { get; set; }

        //Тип ракеты 3
        public string projectile3 { get; set; }

        //Тип ракеты 4
        public string projectile4 { get; set; }

        [JsonConstructor] //Если этот конструктор меняете, то сообщаете об этом тому, кто занимается панелью управления. Это важно!!!
        public Task(string laType, string laSpeed, string azimuth, string aimData, string weather,
            string noisesType, string noisesTime, string map, string tragectory, string startRange, string startHeight,
            string projectile1, string projectile2, string projectile3, string projectile4)
        {
            this.laType = laType;
            this.laSpeed = laSpeed;
            this.azimuth = azimuth;
            this.aimData = aimData;
            this.weather = weather;
            this.noisesType = noisesType;
            this.noisesTime = noisesTime;
            this.map = map;
            this.tragectory = tragectory;
            this.startRange = startRange;
            this.startHeight = startHeight;
            this.projectile1 = projectile1;
            this.projectile2 = projectile2;
            this.projectile3 = projectile3;
            this.projectile4 = projectile4;
            SetupTask();
            Manager.Result = new Dictionary<string, string>();
            Manager.Result.Add("Местность:", map);
            Manager.Result.Add("Погодные условия:", weather);
            Manager.Result.Add("Тип ЛА:", laType);
            Manager.Result.Add("Скорость ЛА:", laSpeed);
            Manager.Result.Add("Траектория:", tragectory);
            Manager.Result.Add("Помехи:", noisesType);
            Manager.Result.Add("Ракета 1:", projectile1);
            Manager.Result.Add("Ракета 2:", projectile2);
            Manager.Result.Add("Ракета 3:", projectile3);
            Manager.Result.Add("Ракета 4:", projectile4);
            //"laType":"Тип A10A","laSpeed":"200",
            //"azimuth":"20","aimData":"Без","weather":"Безоблачно","noisesType":"Без помех","map":"Земляная",
            //"tragectory":"Усложненная","startRange":"9000","startHeight":"50","projectile1":"9М37",
            //"projectile2":"9М37","projectile3":"9М37","projectile4":"9М37"
        }

        private void SetupTask()
        {
            this.rockets = TaskUtil.CreateRockets(projectile1, projectile2, projectile3, projectile4);
            Enemy enemy = TaskUtil.CreateEnemy(laType);

            if (enemy != null)
            {
                EnemyList = new List<Enemy>();
                EnemyList.Add(enemy);

                enemy.mapPath = TaskUtil.CreateEnemyTrajectory(tragectory);
            }
            else
            {
                throw new ArgumentNullException("Enemy is not spotted");
            }

            this.scene = TaskUtil.CreateScene(map);
            this.skybox = TaskUtil.CreateWeather(weather);

            instance = new Task(skybox, Scene, false, EnemyList, rockets);
        }

        public Task(int sky, int map, bool fog, List<Enemy> enemys, List<RocketTypes> rocketTypes)
        {
            this.Skybox = sky;
            this.Scene = map;

            this.EnemyList = enemys;
            this.Fog = fog;

            if (rocketTypes == null)
            {
                this.Rockets = new List<RocketTypes>();
                Rockets.Add(RocketTypes.M333);
                Rockets.Add(RocketTypes.M37);
                Rockets.Add(RocketTypes.M37M);
                Rockets.Add(RocketTypes.M333);
            }
            else
            {
                Rockets = rocketTypes;
            }

            //instance = this;
        }

        public void SetCurrentTask(string laType, string laSpeed, string azimuth, string aimData, string weather,
            string noisesType, string noisesTime, string map, string tragectory, string startRange, string startHeight,
            string projectile1, string projectile2, string projectile3, string projectile4)
        {
            this.laType = laType;
            this.laSpeed = laSpeed;
            this.azimuth = azimuth;
            this.aimData = aimData;
            this.weather = weather;
            this.noisesType = noisesType;
            this.noisesTime = noisesTime;
            this.map = map;
            this.tragectory = tragectory;
            this.startRange = startRange;
            this.startHeight = startHeight;
            this.projectile1 = projectile1;
            this.projectile2 = projectile2;
            this.projectile3 = projectile3;
            this.projectile4 = projectile4;
            //TODO : some logic
        }

        public static Task GetCurrentTask()
        {
            return instance;
        }
    }
}
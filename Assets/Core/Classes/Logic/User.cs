﻿using System;
using System.IO;
using UnityEngine;

namespace Assets.Core.Classes.Logic
{
    [Serializable]
    public class User
    {
        public string name { get; set; }

        public string squad { get; set; }

        [NonSerialized]
        private static User instance;

        private User()
        { }

        public static User GetInstance()
        {
            if (instance == null)
            {
                instance = new User();
            }

            return instance;
        }

        public void Set(string name, string squad)
        {
            this.name = name;
            this.squad = squad;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quobject.SocketIoClientDotNet.Client;
using UnityEngine;

namespace Assets.Core.Classes.Logic
{
	class Config {

		private String ipPrivate;
		private int portPrivate;

		public String ip {
			set {
				ipPrivate = value;
			}

			get {
				return ipPrivate;
			}
		}

		public int port  {
			
			set {
				portPrivate = value;
			}

			get {
				return portPrivate;
			}
		}

		public Config(){
		}

		public String toString(){
			return String.Format ("ip: {0} port: {1}", this.ip, this.port);
		}
	}
}

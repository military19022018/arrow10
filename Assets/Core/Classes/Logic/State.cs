﻿using System.Collections;
using System.Collections.Generic;

namespace App
{
    public static class State
    {

        // STATE VARIABLES 

        private static bool _BOEVOY_UCHEBNIY;
        public static bool BOEVOY_UCHEBNIY { get { return _BOEVOY_UCHEBNIY; } set { _BOEVOY_UCHEBNIY = value; } }

        /** Состояние гашетки.
        * 
        * false >> не нажата.
        * true  >> нажата.
        */
        private static bool _GASHETKA;
        public static bool GASHETKA { get { return _GASHETKA; } set { _GASHETKA = value; } }

        /** Положение кнопки БОРТ
        * 
        * false >> не нажата.
        * true  >> нажата.
        */
        private static bool _BORT;
        public static bool BORT { get { return _BORT; } set { _BORT = value; } }

        private static int _slejenie_pusk;
        public static bool setSLEJENIE_PUSK(int value)
        {
            switch (_slejenie_pusk)
            {
                case 0:
                    if (value == 2)
                    {
                        return false;
                    }
                    // СЛЕЖЕНИЕ-ПУСК отпущена
                    // отпустить кнопку можно в любое время.
                    _slejenie_pusk = value;
                    break;
                case 1:
                    _slejenie_pusk = value;
                    break;
                case 2:
                    if (_slejenie_pusk == 0)
                    {
                        return false;
                    }
                    _slejenie_pusk = value;
                    break;
            }
            return true;
        }
        public static int SLEJENIE_PUSK { get { return _slejenie_pusk; } }

        private static int _pryvod_vykl_ruchnoe;
        public static bool setPRYVOD_VYKL_RUCHNOE(int value)
        {
            switch (_pryvod_vykl_ruchnoe)
            {
                case 0:
                    if (value == 2)
                    {
                        return false;
                    }
                    _pryvod_vykl_ruchnoe = value;
                    break;
                case 1:
                    _pryvod_vykl_ruchnoe = value;
                    break;
                case 2:
                    if (_pryvod_vykl_ruchnoe == 0)
                    {
                        return false;
                    }
                    _pryvod_vykl_ruchnoe = value;
                    break;
            }
            return true;
        }
        public static int PRYVOD_VYKL_RUCHNOE { get { return _pryvod_vykl_ruchnoe; } }


        /** Положение тумблера ПСП
        * 
        * false >> ВЫКЛ.
        * true  >> ВКЛ.
        */
        private static bool _PSP;
        public static bool PSP { get { return _PSP; } set { _PSP = value; } }

        /** Положение тумблера РЕЖИМ ПСП
        * 
        * false >> ДП.
        * true  >> НП.
        */
        private static bool _PSP_mode;
        public static bool PSP_mode { get { return _PSP_mode; } set { _PSP_mode = value; } }

        /** Положение тумблера СОС
        * 
        * false >> выключен.
        * true  >> включен.
        */
        private static bool _SOS;
        public static bool SOS { get { return _SOS; } set { _SOS = value; } }


        // Тумблеры питания 24 и 28 В
        private static bool _V24;
        public static bool V24
        {
            get { return _V24; }
            set
            {
                _V24 = value;
            }
        }

        private static bool _V28;
        public static bool V28
        {
            get { return _V28; }
            set
            {
                _V28 = value;
            }
        }

        /** Аппаратура оценки зоны
        * false >> выключена.
        * true  >> включена.
        */
        private static bool _AOZ;
        public static bool AOZ { get { return _AOZ; } set { _AOZ = value; } }

        /** Положение тумблера ФОН
        * 
        * false >> в положении ФОН-1 
        * true  >> в положении ФОН-2 (используется для селекции помех).
        */
        private static bool _FON;
        public static bool FON { get { return _FON; } set { _FON = value; } }


        /** Тип положений РОД РАБОТЫ
         * 
         * 
         */
        public enum ROD_RABOTY_TYPE { AUTO = 0, R1 = 1, R2 = 2, R3 = 3, R4 = 4 };

        /** положение тумблера РОД РАБОТЫ
         * 
         * AUTO  >> авто.
         * 1     >> 1.
         * 2     >> 2.
         * 3     >> 3.
         * 4     >> 4.
         */
        private static ROD_RABOTY_TYPE _ROD_RABOTY = ROD_RABOTY_TYPE.AUTO;
        public static ROD_RABOTY_TYPE ROD_RABOTY
        {
            get { return _ROD_RABOTY; }
            set
            {
                _ROD_RABOTY = value;
            }
        }


        // Состояние текущей ракеты.
        private static int current_rocket;
        public static int Current_rocket
        {
            get
            {

                switch (ROD_RABOTY)
                {
                    case ROD_RABOTY_TYPE.R1:
                        current_rocket = 0;
                        break;
                    case ROD_RABOTY_TYPE.R2:
                        current_rocket = 1;
                        break;
                    case ROD_RABOTY_TYPE.R3:
                        current_rocket = 2;
                        break;
                    case ROD_RABOTY_TYPE.R4:
                        current_rocket = 3;
                        break;
                    default:
                        if (R1 == RocketTypes.NONE)
                            current_rocket = 1;
                        if (R2 == RocketTypes.NONE)
                            current_rocket = 2;
                        if (R3 == RocketTypes.NONE)
                            current_rocket = 3;
                        if (R4 == RocketTypes.NONE)
                            current_rocket = -1;
                        break;
                        //return -1;
                }
                return current_rocket;
            }
            set { current_rocket = value; }
        }

        // Переменные под ракеты.
        private static RocketTypes _R1;
        public static RocketTypes R1
        {
            get { return _R1; }
            set
            {
                _R1 = value;
            }
        }

        private static RocketTypes _R2;
        public static RocketTypes R2
        {
            get { return _R2; }
            set
            {
                _R2 = value;
            }
        }

        private static RocketTypes _R3;
        public static RocketTypes R3
        {
            get { return _R3; }
            set
            {
                _R3 = value;
            }
        }

        private static RocketTypes _R4;
        public static RocketTypes R4
        {
            get { return _R4; }
            set
            {
                _R4 = value;
            }
        }

        /** Тумблер Охлаждение
        *   (включается при стрельбе в ИК канале)
        * false >> выключен.
        * true  >> включен.
        */
        private static bool _CHILL;
        public static bool CHILL { get { return _CHILL; } set { _CHILL = value; } }

        /** Тумблер ИСПЫТ. ОСНОВ.
        * false >> ИСПЫТ.
        * true  >> ОСНОВ.
        */
        private static bool _ISPYT_OSNOV;
        public static bool ISPYT_OSNOV { get { return _ISPYT_OSNOV; } set { _ISPYT_OSNOV = value; } }

        /** Положение ручки СТОПОР ПОХОД.
        * false >> застопорен.
        * true  >> расстопорен.
        */
        private static bool _STOPOR_POHOD;
        public static bool STOPOR_POHOD { get { return _STOPOR_POHOD; } set { _STOPOR_POHOD = value; } }

        /** Положение тумблера ПЕРЕВОД на ПО2
        * false >> походное.
        * true  >> боевое.
        */
        private static bool _PEREVOD;
        public static bool PEREVOD { get { return _PEREVOD; } set { _PEREVOD = value; } }

        /** Положение тумблера ВЕНТИЛЯТОР на ПО2
        * false >> выключен.
        * true  >> включен.
        */
        private static bool _VENTILYATOR;
        public static bool VENTILYATOR { get { return _VENTILYATOR; } set { _VENTILYATOR = value; } }

        /** Положение тумблера ОБОГРЕВ СТЕКЛА на ПО2
        * false >> выключен.
        * true  >> включен.
        */
        private static bool _OBOGREV_STEKLA;
        public static bool OBOGREV_STEKLA { get { return _OBOGREV_STEKLA; } set { _OBOGREV_STEKLA = value; } }

        /** Положение тумблера ОСВЕЩЕНИЕ на ПО2
        * false >> выключен.
        * true  >> включен.
        */
        private static bool _OSVESHENIYE;
        public static bool OSVESHENIYE { get { return _OSVESHENIYE; } set { _OSVESHENIYE = value; } }

        /** Положение тумблера ОМЫВАТЕЛЬ на ПО2
        * false >> выключен.
        * true  >> включен.
        */
        private static bool _OMYVATEL;
        public static bool OMYVATEL { get { return _OMYVATEL; } set { _OMYVATEL = value; } }

        /** Положение тумблера СЛЕЖЕНИЕ АВТОМАТ. на ПО2
        * false >> выключен.
        * true  >> включен.
        */
        private static bool _SLEJENIE_AVTOMAT;
        public static bool SLEJENIE_AVTOMAT { get { return _SLEJENIE_AVTOMAT; } set { _SLEJENIE_AVTOMAT = value; } }

        /** Положение кнопки СЛЕЖЕНИЕ-ПУСК
        * 
        * 0 >> не нажата (отпущена).
        * 1 >> нажата до первого упора.
        * 2 >> нажата до второго упора.
        */
        //public uint SLEJENIE_PUSK = 0;

        /** Текущее состояние кратности визира
        * 
        * 0 >> используется визир грубой наводки.
        * 1 >> кратность 1,8х  (до цели менее 6 км)
        * 2 >> кратность 3,75х (до цели более 6 км)
        */
        public static uint VISIR = 1;

        // FUNCTIONS

        private static int _PU_angle;
        public static int set_PU_angle
        {
            get { return _PU_angle; }
            set
            {
                _PU_angle = value;
            }
        }


        /** Текущее состояние ПУ по азимуту
         * 
         * int >> угол поворота ПУ
         */
        private static int _PU_azimuth;
        public static int set_PU_azimuth
        {
            get { return _PU_azimuth; }
            set
            {
                if (GASHETKA)
                {
                    _PU_azimuth = value;

                    // азимут может быть от 0 до 360 градусов
                    while (_PU_azimuth >= 360)
                    {
                        _PU_azimuth -= 360;
                    }
                    while (_PU_azimuth < 0)
                    {
                        _PU_azimuth += 360;
                    }
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quobject.SocketIoClientDotNet.Client;
using Newtonsoft.Json;
using UnityEngine;

namespace Assets.Core.Classes.Logic
{
    class Manager
    {
        public static bool customPathValues;
        private static Manager manager;

        private Manager()
        {
        }

        public static Manager getInstance()
        {
            if (manager == null)
            {
                manager = new Manager();
            }
            return manager;
        }

        public static List<int> speedsList = new List<int>() {0,0,0,0};
        public static List<int> heightsList = new List<int>() {0,0,0,0};
        public static int kursParam;

        public static String CUType = "";

        public static bool isPedalPressed = false;

        public static bool isStudyMode;
        public static int stepNumber = 1;

        //public static int startTargetHeight = 1000;
        public static int startTargetFar = 15000; // 15000 = 7500м
        public static int startAzimut = 90;

        public static Dictionary<string, string> Result { get; set; }
        public static Gun visor;        
        public static bool isEnd;
        public static Enemy enemy;
        public static List<Thumbler> thumblers = new List<Thumbler>();
        public static User user;
        public static Task task;
        public static bool po1State { get; set; }
        public static bool po2State { get; set; }
        public static bool visorState { get; set; }
        public static bool uaState { get; set; }
        public static bool glassState { get; set; }
        public static Network network;
        public static string endMessage;
        public static Marka Marka { get; set; }

        public static bool enemyVisible {get; set;}

        public static bool BortPressed { get; set; }
        public static bool SlezheniePressed { get; set; }



        public static void startTask(Task task)
        {
            isPedalPressed = false;
            Gun.bortActivated = false;
            thumblers.Clear();
            Manager.task = task;
            Application.LoadLevel(Manager.task.Scene);
        }

        public static void endTask()
        {
            isEnd = true;
            Application.LoadLevel(0);

            Network.GetInstance().Socket.Emit("taskCompleted", JsonConvert.SerializeObject(SaveUtil.BuildResult()));
            Assets.Core.Classes.Logic.Network.CloseSocket();
            SaveUtil.BuildResult();
        }

        //public static void setState(State newState){                                              /// DELETE IF WORK WITHOUT THIS
        //    state = newState;
        //}

        public static bool shootingAccess()
        {
            try
            {
                //TODO : Раскомментить при показе
                foreach (var item in thumblers)
                {
                    if (item is TwoStatesThumbler)
                    {
                        CheckTwoStates(item as TwoStatesThumbler);
                    }
                    if (item.Name.Equals("privod"))
                    {
                        CheckPrivod();
                    }
                    //if (item.Name.Equals("fon"))
                    //{
                    //    Check
                    //}
                }

                CheckBort();
                //CheckSlezhenie();
            }
            catch (ArgumentException ae)
            {
                return false;
            }
            return visorState;
        }

        private static bool CheckTwoStates(TwoStatesThumbler item)
        {
            if (item.State.Equals(TwoStates.OFF))
            {
                throw new ArgumentException();
            }
            return true;
        }

        private static bool CheckPrivod()
        {
            if (((ThreeStatesThumbler)thumblers.Find(e => e.Name.Equals("privod"))).State.Equals(ThreeStates.MIDDLE))
            {
                throw new ArgumentException();
            }
            return true;
        }

        private static bool CheckBort()
        {
            if (BortPressed)
            {
                return true;
            }
            throw new ArgumentException();
        }

        private static bool CheckSlezhenie()
        {
            var enemyPosition = enemy.transform.position;
            var slezhenieActive = new Vector3(enemyPosition.x + 100, enemyPosition.y + 100, enemyPosition.z + 100);
            if (SlezheniePressed && BortPressed)
            {
                return true;
            }
            throw new ArgumentException();
        }
    }
}
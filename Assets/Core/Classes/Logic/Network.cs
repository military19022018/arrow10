﻿using System;
using System.IO;
using System.Collections.Generic;
using Quobject.SocketIoClientDotNet.Client;
using Newtonsoft.Json;
using UnityEngine;


namespace Assets.Core.Classes.Logic
{
	public class Network
	{
		private static Network instance;
		private Config config;
		public Socket Socket { get; private set; }

		private Network ()
		{
			String configLocation = Application.dataPath + "/config.cfg";
			
			using (StreamReader file = File.OpenText(configLocation))
			{
				JsonSerializer serializer = new JsonSerializer();
				config = (Config) serializer.Deserialize(file, typeof(Config));
			}

			Socket = IO.Socket("http://" + config.ip + ":" + config.port);
		}

		public static Network GetInstance(){
			if (instance == null) {
				instance = new Network ();
			}

			return instance;
		}

		//Возможно в метод Run() придется передать сцену как аргумент, чтобы меню с
		//"ожиданием ответа от сервера" менялось сразу после прихода ответа
		public void Run(String name, String squad)
        {
			Socket.On(Socket.EVENT_CONNECT, () => {

				// При подключении отправяем информацию о своем статусе
				// status = student
				Socket.Emit("status", "{\"status\":\"student\"}");
			});

			Socket.On ("login", () => {
				User user = User.GetInstance();
                user.Set(name, squad);
				Socket.Emit("login", JsonConvert.SerializeObject(user));
			});



			Socket.On (Socket.EVENT_DISCONNECT, () => {
				
				//TODO : Перенести это событие туда, где будет завершаться задание!!!
				Dictionary<string, string> result = new Dictionary<string, string>();
				for (int i = 1; i < 11; i++) {
					//TODO : some logic
					result.Add("task" + i + " ", "resut" + i);
				}
				//Экземпляр сокета можно получить из любого класса приложения
				//Network.GetInstance().Socket.Emit(........);
				Socket.Emit("taskCompleted", JsonConvert.SerializeObject(result));

			});
		}

        public static bool IsNetworkActive()
        {
            return instance == null;
        }

        public static void CloseSocket()
        {
            if (instance != null)
            {
                instance.Socket.Close();
            }
        }

	}
}


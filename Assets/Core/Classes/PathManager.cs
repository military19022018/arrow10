﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathManager : MonoBehaviour
{

	public  EnemyPath[] paths; 

	public EnemyPath getPathByIndex(int index){

		if (index >= paths.Length) {
			return null;
		}

		return paths [index];
	}

	// Use this for initialization
	void Start ()
	{

		paths = transform.GetComponentsInChildren<EnemyPath> ();

	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	void OnDrawGizmos(){

		paths = transform.GetComponentsInChildren<EnemyPath> ();

		Color gizmosColor = Color.white;

		for (int i = 0, length = paths.Length; i < length; i++) {

			EnemyPath currentEPath = this.getPathByIndex (i);
			Gizmos.color = gizmosColor;
			EPPoint prevPoint = null;


			for(int j = 0, length_j = currentEPath.points.Length; j < length_j; j++){

				EPPoint currentPoint = currentEPath.points [j];

				Gizmos.DrawWireSphere (currentPoint.position, 2.0f);


				if (prevPoint == null) {
					prevPoint = currentPoint;
					continue;
				}

				Gizmos.DrawLine (prevPoint.position, currentPoint.position);

				prevPoint = currentPoint;
			}



		}

	}
}


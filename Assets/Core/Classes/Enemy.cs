﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor;
using Assets.Core.Classes.Logic;
using System.Threading;
using System.Diagnostics;
using Debug = UnityEngine.Debug;

public class Enemy : MonoBehaviour {

    public Enemy() { }
    //public Enemy(Enemy e)
    //{
    //    this.id = e.id;
    //    this.type = e.type;
    //    this.speed = e.speed;
    //    this.rotationSpeed = e.rotationSpeed;
    //    this.contrast = e.contrast;
    //    this.temperature = e.temperature;
    //    this.mapPath = e.mapPath;
    //    this.path = e.path;
    //    this.explosion = e.explosion;
    //}

    public Vector3 vectorToGun;
    public double y;
    public double z;

    private IEnumerator coroutine;

    public int id;
    public int type;

    //public float speed;
    public float rotationSpeed;
    public float contrast;
    public float temperature;

    public int mapPath;
    public double timer;

    // public MeshRenderer rend;
    public EnemyPath path;

    private bool flag = true;
    
    public ParticleSystem explosion;

    void Start()
    {
        explosion = Instantiate(Resources.Load<ParticleSystem>("Explosion"));
    }
    
    void Update()
    {
        Gun[] guns = Resources.FindObjectsOfTypeAll<Gun>();
        vectorToGun = guns[0].visorCamera.WorldToScreenPoint(transform.position);
        
        EPPoint ePPoint = path.nextPoint(gameObject.transform.position);

        if (ePPoint != null)
        {
            if (timer < 0 && flag && ePPoint.delay > 0)
            {
                timer = ePPoint.delay;
                flag = false;
            }
        }

        if (ePPoint == null)
        {
            hide();
            return;
        }

        timer -= Time.deltaTime;
        if (timer < 0 || timer == 0)
        {
            transform.position = Vector3.MoveTowards(
                transform.position,
                ePPoint.position,
                (float)Time.deltaTime * ePPoint.speedToPoint
            );
        }

        if (ePPoint != null)
        {
            if (!ePPoint.nonRotate)
            {
                Quaternion r = Quaternion.LookRotation(ePPoint.position - transform.position);
                transform.rotation = Quaternion.Slerp(transform.rotation, r, Time.deltaTime * rotationSpeed);
            }
         
            if (ePPoint.attackEnemy)
            {
                Enemy[] enemies = Resources.FindObjectsOfTypeAll<Enemy>();
                EnemyRocket enemyRocket = Instantiate(Resources.Load<EnemyRocket>("Rocket-Apache"), enemies[0].transform.position, enemies[0].transform.rotation);
                GameObject respawnPoint = GameObject.Find("RespawnPoint");
                enemyRocket.target = respawnPoint;
                enemyRocket.attack();
                ePPoint.attackEnemy = false;
            }
        }



        Manager.enemy = this;

    }

    public void die()
    {
        //TODO send to Manager
        Instantiate(explosion, gameObject.transform.position, Quaternion.identity);
        //StartCoroutine(LoadLevelAfterDelay(4));
        SaveUtil.GetResultsThumblers(Manager.Result);



        if(!Manager.isStudyMode)
        {
            Manager.Result.Add("Тестирование: ", "ПРОЙДЕНО");
            Manager.endMessage = "Вы успешно прошли тренировку!";
            SaveUtil.BuildResult();
        }
        else 
        {
            Manager.Result.Add("ОБУЧЕНИЕ: ", "УСПЕШНО ЗАВЕРШЕНО");
            Manager.endMessage = "Вы успешно завершили обучение!";
        }
        Destroy(gameObject);
        //Manager.endTask();
    }

    void OnApplicationQuit()
    {
        if (!Assets.Core.Classes.Logic.Network.IsNetworkActive())
        {
            Assets.Core.Classes.Logic.Network.CloseSocket();
        }
    }

    public void hide()
    {
        Destroy(gameObject);
        SaveUtil.GetResultsThumblers(Manager.Result);
        if (!Manager.isStudyMode)
        {
            Manager.Result.Add("Тестирование: ", "НЕ ПРОЙДЕНО");
            Manager.endMessage = "Вы не прошли тренировку!";
            SaveUtil.BuildResult();
        }
        else
        {
            Manager.Result.Add("ОБУЧЕНИЕ: ", "ПРОВАЛЕНО");
            Manager.endMessage = "У Вас не получилось завершить ОБУЧЕНИЕ! Начните заново!";
        }
        Manager.endTask();
    }

    private void OnBecameVisible()
    {
        Manager.enemy = this;
        Manager.enemyVisible = true;
    }

    private void OnBecameInvisible()
    {
        Manager.enemy = this;
        Manager.enemyVisible = false;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteChanger : MonoBehaviour {

    public Camera cam;
    
    public bool camback = false;

    public Sprite DSCF1362, pn, po1, po2, vizir;

	// Use this for initialization
	void Start () {
        this.GetComponent<SpriteRenderer>().sprite = DSCF1362;
    }
	
	// Update is called once per frame
	void Update () {
        //mousepos();

        Vector3 mouseposition = Input.mousePosition;
        this.GetComponent<SpriteRenderer>().sprite = DSCF1362;
        if (mouseposition.x > 105 && mouseposition.x < 370 && mouseposition.y > 178 && mouseposition.y < 518)
        {
            this.GetComponent<SpriteRenderer>().sprite = po1;
            if (Input.GetMouseButtonDown(0))
            {
                FocusOnPO1();
                camback = true;
            }
        }

        if (mouseposition.x > 352 && mouseposition.x < 452 && mouseposition.y > 410 && mouseposition.y < 610)
        {
            this.GetComponent<SpriteRenderer>().sprite = po2;
            if (Input.GetMouseButtonDown(0))
            {
                FocusOnPO2();
                camback = true;
            }
        }

        if (mouseposition.x > 612 && mouseposition.x < 1012 && mouseposition.y > 570 && mouseposition.y < 768)
        {
            this.GetComponent<SpriteRenderer>().sprite = vizir;
            if (Input.GetMouseButtonDown(0))
            {
                
                gameObject.active = false;
                Gun[] guns = Resources.FindObjectsOfTypeAll<Gun>();
                guns[0].gameObject.active = true;
                camback = true;
            }
        }

        if (Input.GetKey(KeyCode.Escape) && camback == true)
        {
            this.GetComponent<SpriteRenderer>().sprite = DSCF1362;
            cam.transform.position = new Vector3((float)348.8, (float)49.8, (float)280.8);
            camback = false;
        }

        
    }

    void mousepos()
    {
        Vector3 mouseposition = Input.mousePosition;

        if (Input.GetMouseButtonDown(0))
        {
        }
 
    }

    private void FocusOnPO1()
    {
        cam.transform.position = new Vector3((float)348.8, (float)49.8, (float)273.1);
    }

    private void FocusOnPO2()
    {
        cam.transform.position = new Vector3((float)348.8, (float)49.8, (float)262.5);
    }

}

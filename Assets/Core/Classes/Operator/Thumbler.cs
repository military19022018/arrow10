﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class Thumbler
{
    public string Name { get; set; }
    //public T State { get; set; }

    public abstract void Swap();

    public abstract void Reset();
}
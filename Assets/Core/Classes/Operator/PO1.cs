﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Assets.Core.Classes.Logic;
using UnityEngine.UI;
using System.Linq;

public class PO1 : MonoBehaviour {
    
    public Camera cam;
    public Text step1;
    public Text step2;
    public Text step3;
    public Text step4;
    public Text step5;
    public Text step5_5;
    public Text step6;
    public Text step7;
    public Text step8;



    public TwoStatesThumbler t_24v { get; private set; }
    public TwoStatesThumbler t_28v { get; private set; }
    public TwoStatesThumbler t_aoz { get; private set; }
    public ThreeStatesThumbler t_privod { get; private set; }
    public ThreeStatesThumbler t_fon { get; private set; }
    public FiveStatesThumbler t_rodraboti { get; private set; }
    //public TwoStatesThumbler t_perevod;

    //private BoxCollider bc;

    // Use this for initialization
    void Start()
    {
        //bc = (BoxCollider)GetComponent<BoxCollider>();
        t_24v = new TwoStatesThumbler("Тумблер 24В", TwoStates.OFF);
        t_28v = new TwoStatesThumbler("Тумблер 28В", TwoStates.OFF);
        t_aoz = new TwoStatesThumbler("Тумблер АОЗ", TwoStates.OFF);
        t_privod = new ThreeStatesThumbler("Тумблер Привод", ThreeStates.MIDDLE);
        t_fon = new ThreeStatesThumbler("Тумблер Фон", ThreeStates.BOTTOM);
        t_rodraboti = new FiveStatesThumbler("rodraboti", FiveStates.AUTO);
        //t_perevod = (TwoStatesThumbler)Manager.thumblers.Find(t => t.Name.Equals("Тумблер Перевод"));
        Manager.thumblers.Add(t_24v);
        Manager.thumblers.Add(t_28v);
        Manager.thumblers.Add(t_aoz);
        Manager.thumblers.Add(t_privod);
        //Manager.thumblers.Add(t_fon);
        //Manager.thumblers.Add(t_rodraboti);

    }
    
	// Update is called once per frame
	void Update () 
    {
        var voltmeter = Resources.FindObjectsOfTypeAll<GameObject>()
                                 .First(g => g.name.Equals("volt"));
        var pohod = Resources.FindObjectsOfTypeAll<GameObject>()
                                  .First(g => g.name.Equals("pohod"));
        var post1 = Resources.FindObjectsOfTypeAll<GameObject>()
                                  .First(g => g.name.Equals("1-post"));
        var post2 = Resources.FindObjectsOfTypeAll<GameObject>()
                                  .First(g => g.name.Equals("2-post"));
        var post3 = Resources.FindObjectsOfTypeAll<GameObject>()
                                  .First(g => g.name.Equals("3-post"));
        var post4 = Resources.FindObjectsOfTypeAll<GameObject>()
                                  .First(g => g.name.Equals("4-post"));
        var boevoi = Resources.FindObjectsOfTypeAll<GameObject>()
                                  .First(g => g.name.Equals("boevoi"));
        var bort = Resources.FindObjectsOfTypeAll<GameObject>()
                                  .First(g => g.name.Equals("bort"));
        /*
         * ВКЛЮЧЕНИЕ ПОДСВЕТКИ ВОЛЬТМЕТРА
         */
        var perevod = (TwoStatesThumbler)Manager.thumblers.Find(t => t.Name.Equals("Тумблер Перевод"));
        if (t_24v.State == TwoStates.ON &&
            t_28v.State == TwoStates.ON && 
            t_privod.State != ThreeStates.MIDDLE &&
            Manager.isPedalPressed &&
            perevod.State == TwoStates.ON)
        {
            voltmeter.gameObject.SetActive(true);
        }

        if (t_24v.State == TwoStates.ON &&
            t_28v.State == TwoStates.ON) {
            
            // БОРТ
            if (Gun.bortActivated)
            {
                bort.gameObject.SetActive(true);
            }
            else 
            {
                bort.gameObject.SetActive(false);
            }

            // ПОХОД
            if (t_privod.State == ThreeStates.TOP)
            {
                pohod.gameObject.SetActive(true);
            }
            else
            {
                pohod.gameObject.SetActive(false);
            }

            // БОЕВОЕ
            if ((t_privod.State == ThreeStates.TOP || t_privod.State == ThreeStates.BOTTOM) &&
                     PO2.t_perevod.State == TwoStates.ON &&
                    Manager.isPedalPressed)
            {
                pohod.gameObject.SetActive(false);
                boevoi.gameObject.SetActive(true);
                post1.gameObject.SetActive(true);
            }
            else 
            {
                pohod.gameObject.SetActive(true);
                boevoi.gameObject.SetActive(false);
                post1.gameObject.SetActive(false);
            }



        }
        else
        {
            voltmeter.gameObject.SetActive(false);
        }



        /*
         * РЕЖИМ "ОБУЧЕНИЕ"
        */
        if (Manager.isStudyMode)
        {
            if (t_24v.State == TwoStates.ON && 
                t_28v.State == TwoStates.ON)
            {
                Manager.stepNumber = 2;
            }

            if (t_24v.State == TwoStates.ON &&
                t_28v.State == TwoStates.ON && 
                t_privod.State == ThreeStates.TOP)
            {
                Manager.stepNumber = 3;
            }

            if (t_24v.State == TwoStates.ON &&
                t_28v.State == TwoStates.ON &&
                t_privod.State == ThreeStates.TOP &&
                t_aoz.State == TwoStates.ON)
            {
                Manager.stepNumber = 4;
            }

            step1.gameObject.SetActive(false);
            step2.gameObject.SetActive(false);
            step3.gameObject.SetActive(false);
            step4.gameObject.SetActive(false);
            step5.gameObject.SetActive(false);
            step5_5.gameObject.SetActive(false);
            step6.gameObject.SetActive(false);
            step7.gameObject.SetActive(false);
            step8.gameObject.SetActive(false);

            switch (Manager.stepNumber)
            {
                case 1:
                    step1.gameObject.SetActive(true);
                    break;
                case 2:
                    step2.gameObject.SetActive(true);
                    break;
                case 3:
                    step3.gameObject.SetActive(true);
                    break;
                case 4:
                    step4.gameObject.SetActive(true);
                    break;
                case 5:
                    step5.gameObject.SetActive(true);
                    break;
                case 6:
                    step5_5.gameObject.SetActive(true);
                    break;
                case 7:
                    step6.gameObject.SetActive(true);
                    break;
                case 8:
                    step7.gameObject.SetActive(true);
                    break;
                case 9:
                    step8.gameObject.SetActive(true);
                    break;
                default:
                    break;
            }
        }

        if (cam.gameObject.active && Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.transform != null)
                {
                    string clickedObjectName = hit.transform.gameObject.name;
                    Debug.Log(clickedObjectName);
                    switch(clickedObjectName){
                        case "24v":
                            //t_24v.Swap();
                            Manager.thumblers.Find(t => t.Name.Equals(t_24v.Name)).Swap();
                            break;
                        case "28v":
                            //t_28v.Swap();
                            Manager.thumblers.Find(t => t.Name.Equals(t_28v.Name)).Swap();
                            break;
                        case "privod":
                            //t_privod.Swap();
                            Manager.thumblers.Find(t => t.Name.Equals(t_privod.Name)).Swap();
                            break;
                        case "aoz":
                            //t_aoz.Swap();
                            Manager.thumblers.Find(t => t.Name.Equals(t_aoz.Name)).Swap();
                            break;
                    }

                }
            }
        }
        if (cam.gameObject.active)
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                Manager.po1State = false;
                GoToOperatorCamera();
            }

        } 
        else
        {
            //bc.enabled = true;
        }
    }

    private void GoToOperatorCamera()
    {
        cam.gameObject.active = false;
        Operator[] ops = Resources.FindObjectsOfTypeAll<Operator>();
        ops[0].cam.gameObject.active = true;

    }

    void OnApplicationQuit()
    {
        if (!Assets.Core.Classes.Logic.Network.IsNetworkActive())
        {
            Assets.Core.Classes.Logic.Network.CloseSocket();
        }
    }

}

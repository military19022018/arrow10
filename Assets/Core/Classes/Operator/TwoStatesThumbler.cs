﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoStatesThumbler : Thumbler
{
    public TwoStates State { get; set; }

    public TwoStatesThumbler(string name, TwoStates startState)
    {
        Name = name;
        State = startState;
    }

    public override void Swap(){
        if (State == TwoStates.OFF)
        {
            State = TwoStates.ON;
        }
        else
        {
            State = TwoStates.OFF;
        }
    }

    public override void Reset()
    {
        this.State = TwoStates.OFF;
    }
}

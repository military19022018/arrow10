﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switcher : MonoBehaviour {

    public int startState = 0;

    private int statePrivate;
    public int state
    {
        get
        {
            return statePrivate;
        }

        set
        {
            if(value < 0 || value >= stateAngles.Count)
            {
                return;
            }

            statePrivate = value;
            transform.rotation = stateAngles[state];
        }
    }


    public List<Quaternion> stateAngles;

    public bool editMode = false;
    public int currentEditState = 0;

	
    public int goNext()
    {
        if(state + 1 == stateAngles.Count)
        {
            state = 0;
        } else
        {
            state++;
        }

        return state;
    }
    
    // Use this for initialization
	void Start () {
        state = startState;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseDown()
    {
        goNext();
    }

    void OnDrawGizmos()
    {

        if (editMode)
        {
            if (currentEditState >= stateAngles.Count) return;

            stateAngles[currentEditState] = transform.rotation;

        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tumbler : MonoBehaviour
{

    public bool startState;

    private bool statePrivate;
    public bool state
    {
        get
        {
            return statePrivate;
        }
        set
        {
            statePrivate = value;
            transform.rotation = (statePrivate ? onAngle : offAngle);
        }
    }

    public bool toggle()
    {
        state = !state;
        return state;
    }

    public Quaternion onAngle;
    public Quaternion offAngle;


    // Use this for initialization
    void Start()
    {

        state = startState;

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnMouseDown()
    {
        toggle();
    }


    public bool setOnRotation = true;
    public bool setOffRotation = false;

    void OnDrawGizmos()
    {

        if (setOnRotation)
        {
            onAngle = transform.rotation;
        }

        if (setOffRotation)
        {
            offAngle = transform.rotation;
        }
    }
}
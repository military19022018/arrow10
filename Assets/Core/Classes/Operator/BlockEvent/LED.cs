﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor;
using Assets.Core.Classes.Logic;


public class LED : MonoBehaviour {

    public bool startState;

    private bool statePrivate;
    public bool state
    {
        get
        {
            return statePrivate;
        }
    }

    public bool toggle()
    {
        if (state)
        {
            turnOff();
        } else
        {
            turnOn();
        }
        return state;
    }

    public bool turnOn()
    {
        
        statePrivate = false;
        return state;
    }

    public bool turnOff()
    {
        statePrivate = true;
        return state;
    }

	// Use this for initialization
	void Start () {
        statePrivate = startState;
        if (startState) turnOn();
        else turnOff();
	}
	
	// Update is called once per frame
	void Update () {
		
	}


}

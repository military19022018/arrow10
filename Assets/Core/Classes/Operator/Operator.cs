﻿using System.Collections;
using System.Collections.Generic;
using Assets.Core.Classes.Logic;
using UnityEngine;
using UnityEngine.UI;

public class Operator : MonoBehaviour {

    public Camera cam;

    private const string tumb_24v = "Тумблер 24В";
    private const string tumb_28v = "Тумблер 28В";
    private const string tumb_aoz = "Тумблер АОЗ";
    private const string tumb_privod = "Тумблер Привод";
    private const string tumb_fon = "Тумблер Фон";
    private const string tumb_rodraboti = "rodraboti";
    private const string tumb_perevod = "Тумблер Перевод";

    public PO1 po1;
    public PO2 po2;
    public UA ua;
    public Glass glass;
    public VisorBlock visor; 
    public SpriteRenderer po1_sprite;
    public SpriteRenderer po2_sprite;
    public SpriteRenderer vizor_sprite;
    public SpriteRenderer ua_sprite;
    public SpriteRenderer glass_sprite;
    public Text step1;
    public Text step2;
    public Text step3;
    public Text step4;
    public Text step5;
    public Text step5_5;
    public Text step6;
    public Text step7;
    public Text step8;

    public TwoStatesThumbler t_24v { get; private set; }
    public TwoStatesThumbler t_28v { get; private set; }
    public TwoStatesThumbler t_aoz { get; private set; }
    public ThreeStatesThumbler t_privod { get; private set; }
    public TwoStatesThumbler t_perevod;


    // Use this for initialization
    void Start () {

        App.State.Current_rocket = -1;
        Manager.visorState = false;
	}
	
	// Update is called once per frame
	void Update () {

        if (Manager.isStudyMode)
        {
            if (Manager.stepNumber == 5)
            {
                t_24v = (TwoStatesThumbler)Manager.thumblers.Find(t => t.Name.Equals(tumb_24v));
                t_28v = (TwoStatesThumbler)Manager.thumblers.Find(t => t.Name.Equals(tumb_28v));
                t_aoz = (TwoStatesThumbler)Manager.thumblers.Find(t => t.Name.Equals(tumb_aoz));
                t_privod = (ThreeStatesThumbler)Manager.thumblers.Find(t => t.Name.Equals(tumb_privod));
                t_perevod = (TwoStatesThumbler)Manager.thumblers.Find(t => t.Name.Equals(tumb_perevod));

                if (t_24v.State == TwoStates.ON &&
                t_28v.State == TwoStates.ON &&
                t_privod.State == ThreeStates.TOP &&
                t_aoz.State == TwoStates.ON &&
                t_perevod.State == TwoStates.ON &&
                Manager.isPedalPressed == true)
                {
                    Manager.stepNumber = 6;
                }
            }



            step1.gameObject.SetActive(false);
            step2.gameObject.SetActive(false);
            step3.gameObject.SetActive(false);
            step4.gameObject.SetActive(false);
            step5.gameObject.SetActive(false);
            step5_5.gameObject.SetActive(false);
            step6.gameObject.SetActive(false);
            step7.gameObject.SetActive(false);
            step8.gameObject.SetActive(false);

            switch (Manager.stepNumber)
            {
                case 1:
                    step1.gameObject.SetActive(true);
                    break;
                case 2:
                    step2.gameObject.SetActive(true);
                    break;
                case 3:
                    step3.gameObject.SetActive(true);
                    break;
                case 4:
                    step4.gameObject.SetActive(true);
                    break;
                case 5:
                    step5.gameObject.SetActive(true);
                    break;
                case 6:
                    step5_5.gameObject.SetActive(true);
                    break;
                case 7:
                    step6.gameObject.SetActive(true);
                    break;
                case 8:
                    step7.gameObject.SetActive(true);
                    break;
                case 9:
                    step8.gameObject.SetActive(true);
                    break;
                default:
                    break;
            }
        }

		
        // Focus on block by click
        if(cam.gameObject.active && Input.GetMouseButtonDown(0))
        {
            //Manager.state.Current_rocket = -1;
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.transform != null)
                {
                    string clickedObjectName = hit.transform.gameObject.name;
                    Debug.Log(clickedObjectName);
                    if (clickedObjectName.Equals(po1_sprite.gameObject.name))
                    {
                        FocusOnPO1();
                    }
                    else if (clickedObjectName.Equals(po2_sprite.gameObject.name))
                    {
                        FocusOnPO2();
                    }
                    else if (clickedObjectName.Equals(vizor_sprite.gameObject.name))
                    {
                        App.State.Current_rocket = 0;
                        GoToVisor();
                    }
                    else if (clickedObjectName.Equals(ua_sprite.gameObject.name))
                    {
                        App.State.Current_rocket = 0;
                        FocusOnUA();
                    }
                    else if (clickedObjectName.Equals(glass_sprite.gameObject.name))
                    {
                        App.State.Current_rocket = 0;
                        FocusOnGlass();
                    }
                    else {
                        // anything else
                    }
                }
            }
        }
	}

    private void FocusOnGlass()
    {
        Manager.glassState = true;
        Manager.visorState = false;
        gameObject.active = false;
        Gun[] guns = Resources.FindObjectsOfTypeAll<Gun>();
        guns[0].gameObject.active = true;
        Manager.visor = guns[0];
        guns[0].transform.rotation = Quaternion.Euler(-25f, guns[0].transform.rotation.eulerAngles.y, 0);
    }

    private void FocusOnUA()
    {
        Manager.uaState = true;
        cam.gameObject.active = false;
        ua.cam.gameObject.active = true;
    }

    void OnApplicationQuit()
    {
        if (!Assets.Core.Classes.Logic.Network.IsNetworkActive())
        {
            Assets.Core.Classes.Logic.Network.CloseSocket();
        }
    }

    private void FocusOnPO1()
    {
        Manager.po1State = true;
        cam.gameObject.active = false;
        po1.cam.gameObject.active = true;

    }

    private void FocusOnPO2()
    {
        Manager.po2State = true;
        cam.gameObject.active = false;
        po2.cam.gameObject.active = true;

    }


    private void GoToVisor()
    {
        Manager.glassState = false;
        Manager.visorState = true;
        gameObject.active = false;
        Gun[] guns = Resources.FindObjectsOfTypeAll<Gun>();
        guns[0].gameObject.active = true;
        Manager.visor = guns[0];
    }
}

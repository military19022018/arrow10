﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PN : MonoBehaviour {

    public Camera cam;
    private BoxCollider bc;

    // Use this for initialization
    void Start()
    {
        bc = (BoxCollider)GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {

        if (cam.gameObject.active)
        {
            bc.enabled = false;

            if (Input.GetKey(KeyCode.Escape))
            {
                GoToOperatorCamera();
            }

        }
        else
        {
            bc.enabled = true;
        }



    }

    void OnApplicationQuit()
    {
        if (!Assets.Core.Classes.Logic.Network.IsNetworkActive())
        {
            Assets.Core.Classes.Logic.Network.CloseSocket();
        }
    }

    private void GoToOperatorCamera()
    {
        cam.gameObject.active = false;
        Operator[] ops = Resources.FindObjectsOfTypeAll<Operator>();
        ops[0].cam.gameObject.active = true;

    }
}

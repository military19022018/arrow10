﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knopka : MonoBehaviour {

    private bool changer = false;

    void Start()
    {

    }

    void OnMouseDrag()
    {
        if (!changer)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 0.25f);
            changer = true;
        }
    }

    void OnMouseUp()
    {
        if (changer)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.25f);
            changer = false;
        }
    }
}

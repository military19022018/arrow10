﻿using System.Collections;
using System.Collections.Generic;
using Assets.Core.Classes.Logic;
using UnityEngine;

public class UA : MonoBehaviour {
    //"UA-STRELKA-BIG"
    public Camera cam;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (cam.gameObject.active)
        {
            GameObject strelka = GameObject.Find("UA-STRELKA-BIG");
            strelka.transform.rotation = Quaternion.Euler(0, 0, Gun.strelkaRotation);
            if (Input.GetKey(KeyCode.Escape))
            {
                Manager.uaState = false;
                GoToOperatorCamera();
            }

        }
    }

    private void GoToOperatorCamera()
    {
        cam.gameObject.active = false;
        Operator[] ops = Resources.FindObjectsOfTypeAll<Operator>();
        ops[0].cam.gameObject.active = true;

    }
}

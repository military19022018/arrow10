﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Core.Classes.Logic;
using UnityEngine.UI;

public class PO2 : MonoBehaviour {

    public Camera cam;
    public Text step1;
    public Text step2;
    public Text step3;
    public Text step4;
    public Text step5;
    public Text step5_5;
    public Text step6;
    public Text step7;
    public Text step8;


    private BoxCollider bc;
    public static TwoStatesThumbler t_perevod;

    // Use this for initialization
    void Start()
    {
        bc = (BoxCollider)GetComponent<BoxCollider>();
        t_perevod = new TwoStatesThumbler("Тумблер Перевод", TwoStates.OFF); // OFF = ПОХОДНОЕ | ON = БОЕВОЕ
        Manager.thumblers.Add(t_perevod);
    }

    // Update is called once per frame
    void Update()
    {

        if (Manager.isStudyMode)
        {
            if (t_perevod.State == TwoStates.ON)
            {
                Manager.stepNumber = 5;
            }


            step1.gameObject.SetActive(false);
            step2.gameObject.SetActive(false);
            step3.gameObject.SetActive(false);
            step4.gameObject.SetActive(false);
            step5.gameObject.SetActive(false);
            step5_5.gameObject.SetActive(false);
            step6.gameObject.SetActive(false);
            step7.gameObject.SetActive(false);
            step8.gameObject.SetActive(false);

            switch (Manager.stepNumber)
            {
                case 1:
                    step1.gameObject.SetActive(true);
                    break;
                case 2:
                    step2.gameObject.SetActive(true);
                    break;
                case 3:
                    step3.gameObject.SetActive(true);
                    break;
                case 4:
                    step4.gameObject.SetActive(true);
                    break;
                case 5:
                    step5.gameObject.SetActive(true);
                    break;
                case 6:
                    step5_5.gameObject.SetActive(true);
                    break;
                case 7:
                    step6.gameObject.SetActive(true);
                    break;
                case 8:
                    step7.gameObject.SetActive(true);
                    break;
                case 9:
                    step8.gameObject.SetActive(true);
                    break;
                default:
                    break;
            }

        }


        if (cam.gameObject.active && Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.transform != null)
                {
                    string clickedObjectName = hit.transform.gameObject.name;
                    Debug.Log(clickedObjectName);
                    switch (clickedObjectName)
                    {
                        case "perevod":
                            t_perevod.Swap();
                            //Manager.thumblers.Find(e => e.Name.Equals(t_perevod.Name)).Swap();
                            break;
                    }

                }
            }
        }
        if (cam.gameObject.active)
        {
            bc.enabled = false;

            if (Input.GetKey(KeyCode.Escape))
            {
                Manager.po2State = false;
                GoToOperatorCamera();
            }

        }
        else
        {
            bc.enabled = true;
        }
    }

    void OnApplicationQuit()
    {
        if (!Assets.Core.Classes.Logic.Network.IsNetworkActive())
        {
            Assets.Core.Classes.Logic.Network.CloseSocket();
        }
    }

    private void GoToOperatorCamera()
    {
        cam.gameObject.active = false;
        Operator[] ops = Resources.FindObjectsOfTypeAll<Operator>();
        ops[0].cam.gameObject.active = true;

    }
}

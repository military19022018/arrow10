﻿using System.Collections;
using System.Collections.Generic;
using Assets.Core.Classes.Logic;
using UnityEngine;

public class Glass : MonoBehaviour {

    const string tumb_24v = "Тумблер 24В";
    const string tumb_28v = "Тумблер 28В";
    public Camera cam;
    private bool gashetkaActive = false;
    float currentCamSpeed;
    float camSpeed = 50;

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {

        var v24 = (TwoStatesThumbler)Manager.thumblers.Find(t => t.Name.Equals(tumb_24v));
        var v28 = (TwoStatesThumbler)Manager.thumblers.Find(t => t.Name.Equals(tumb_28v));

        if (Input.GetKey(KeyCode.Escape))
        {
            Manager.glassState = false;
            gameObject.SetActive(false);
            Operator[] ops = Resources.FindObjectsOfTypeAll<Operator>();
            ops[0].gameObject.SetActive(true);
        }

        currentCamSpeed = camSpeed;
        float currentRot = transform.rotation.eulerAngles.x % 180;


        /*
         * ГАШЕТКА
         */
        if (Input.GetKey(KeyCode.E) &&
           v24.State == TwoStates.ON &&
           v28.State == TwoStates.ON) // 24, 28 
        {
            this.gashetkaActive = true;
        }
        if (Input.GetKeyUp(KeyCode.E))
        {
            this.gashetkaActive = false;
        }


        /*
         * ДВИЖЕНИЕ ВЛЕВО/ВПРАВО
         */
        if (gashetkaActive && Input.GetKey(KeyCode.A))
        {
            transform.RotateAround(cam.transform.position, Vector3.up, -currentCamSpeed * Time.deltaTime);
        }
        else if (gashetkaActive && Input.GetKey(KeyCode.D))
        {
            transform.RotateAround(cam.transform.position, Vector3.up, currentCamSpeed * Time.deltaTime);
        }
    }

    private void GoToOperatorCamera()
    {
        cam.gameObject.active = false;
        Operator[] ops = Resources.FindObjectsOfTypeAll<Operator>();
        ops[0].cam.gameObject.active = true;

    }
}

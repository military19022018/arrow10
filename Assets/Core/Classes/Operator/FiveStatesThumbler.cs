using UnityEngine;
using System.Collections;
using System;

public class FiveStatesThumbler : Thumbler
{
    public FiveStates State { get; set; }
    private bool flag;

    public FiveStatesThumbler(string name, FiveStates state)
    {
        Name = name;
        State = state;
    }

    public override void Swap(){
        if (State == 0)
        {
            flag = true;
            State += 1;
        }
        else if ((int)State == Enum.GetValues(State.GetType()).Length - 1)
        {
            flag = false;
            State -= 1;
        }
        else
        {
            if (flag) State += 1;
            else State -= 1;
        }
    }

    public override void Reset()
    {
        this.State = FiveStates.AUTO;
    }
}

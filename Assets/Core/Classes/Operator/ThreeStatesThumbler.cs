﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThreeStatesThumbler : Thumbler
{
    public ThreeStates State { get; set; }
    private Boolean flag;

    public ThreeStatesThumbler(string name, ThreeStates state)
    {
        Name = name;
        State = state;
    }

    public override void Swap(){
        if (State == ThreeStates.MIDDLE && !flag)
        {
            flag = true;
            State = ThreeStates.TOP;
            return;
        }
        if (State == ThreeStates.MIDDLE && flag)
        {
            flag = false;
            State = ThreeStates.BOTTOM;
        }
        else
        {
            State = ThreeStates.MIDDLE;
        }
    }

    public override void Reset()
    {
        this.State = ThreeStates.MIDDLE;
    }
}


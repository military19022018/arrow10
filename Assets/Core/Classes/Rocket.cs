﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Core.Classes.Logic;
using System;

public class Rocket : MonoBehaviour {

    public int id;
    public RocketTypes type;
    public float speed;
    private bool isAttack = false;
    private bool isPushed = false;

    //private Vector3 lastVector;
    private Enemy targetPrivate; // enemy ID
    public Enemy target
    {
        get {
            return targetPrivate;
        }
        set {
            targetPrivate = value;
        }
    }

	// Use this for initialization
	void Start () {
		
	}

    void OnApplicationQuit()
    {
        if (!Assets.Core.Classes.Logic.Network.IsNetworkActive())
        {
            Assets.Core.Classes.Logic.Network.CloseSocket();
        }
    }

    // Update is called once per frame
    void Update () {
        if (!isAttack)
        {
            return;
        }
        speed += 10;
        if (target == null)
        {
            if (!isPushed)
            {
                GetComponent<Rigidbody>().useGravity = true;

                GetComponent<Rigidbody>().AddForce(transform.forward * speed, ForceMode.Impulse);
                isPushed = true;
            }
            return;
        }
        if (Physics.Linecast(transform.position, target.transform.position))
        {
            Debug.DrawLine(transform.position, target.transform.position);
            target = null;
            return;
        }
        if (Vector3.Distance(gameObject.transform.position, target.transform.position) <= 10)
        {
            target.die(); 
            StartCoroutine(LoadLevelAfterDelay(3));
            die();
            return;
        }

        transform.position = Vector3.MoveTowards(
            transform.position,
            target.gameObject.transform.position,
            (float) Time.deltaTime * speed * 1.01F
        );
        Quaternion r = Quaternion.LookRotation(target.gameObject.transform.position - transform.position);

        transform.rotation = r;
    }

    private static IEnumerator LoadLevelAfterDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            Manager.endTask();
        }
    }

    public void findTarget()
    {
        target = Manager.task.EnemyList[0];
        return;
    }

    public void attack()
    {
        isAttack = true;
    }

    public void die()
    {
        gameObject.transform.position = new Vector3(0,0,0);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Core.Classes.Logic;

public class Map : MonoBehaviour {

	public GameObject respawnPoint;
	public Gun gun;
    public Glass glass;
	public PathManager pathManager;
    public static Enemy enemy;
	private int skyboxPrivate;
    public int skybox
    {
        get
        {
            return skyboxPrivate;
        }

        set
        {
            skyboxPrivate = value;

            RenderSettings.skybox = skyboxs[skyboxPrivate];

            float intensity = 1f;

            // В зависимости от skybox разное освещение
            switch (skyboxPrivate)
            {
                case 1:
                    intensity = 0.8f;
                    break;
                case 2:
                    intensity = 0.5f;
                    break;
            }

            lt.intensity = intensity;

        }
    }

	public Material[] skyboxs;
	public Light lt;

	/*
	 * Skyboxs:
	 * 0 - clear
	 * 1 - someclouds
	 * 2 - darkclouds
	*/

	// Use this for initialization
	void Start () {

        // Загружаем настройки карты
        skybox = Manager.task.Skybox;


        Gun gunClone = Instantiate(gun, respawnPoint.transform.position, respawnPoint.transform.rotation);
        //Operator op = (Operator) Instantiate((Operator) AssetDatabase.LoadAssetAtPath<Operator>("Assets/Prefabs/OperatorPosition/OperatorPosition.prefab"), respawnPoint.transform.position, respawnPoint.transform.rotation);
        Operator op;
        if (Manager.isStudyMode)
        {
            op = Instantiate(Resources.Load<Operator>("OperatorPositionStudy"), respawnPoint.transform.position, respawnPoint.transform.rotation);
        }
        else
        {
            op = Instantiate(Resources.Load<Operator>("OperatorPosition"), respawnPoint.transform.position, respawnPoint.transform.rotation);
        }

        for (int i = 0, length = Manager.task.EnemyList.Count; i < length; i++)
        {
            Enemy en = Manager.task.EnemyList[i];
            EnemyPath ep = pathManager.getPathByIndex(en.mapPath);



            // УСТАНОВКА ВЫСОТЫ
            //ep.transform.position = new Vector3(ep.transform.position.x,
            //                                    Manager.startTargetHeight,
            //                                    ep.transform.position.z);


            // ДАЛЬНОСТЬ ИСХОДНОЙ ТОЧКИ
            Vector3 point0 = ep.points[0].transform.position;
            Gun[] guns = Resources.FindObjectsOfTypeAll<Gun>();
            Vector3 gunPos = guns[0].transform.position;
            Vector3 diff = point0 - gunPos;
            ep.points[0].transform.position = gunPos + diff.normalized * (Manager.startTargetFar / 2);

          

            en.path = ep;
            Manager.task.EnemyList[i] = Instantiate(en, ep.startPoint.position, ep.startPoint.rotation);
            
            GameObject respawnPoint = GameObject.Find("RespawnPoint");
            Vector3 relativePos = new Vector3(Manager.task.EnemyList[i].transform.position.x - guns[0].transform.position.x, 
                                              0,
                                              Manager.task.EnemyList[i].transform.position.z - guns[0].transform.position.z);
            Quaternion rotation = Quaternion.LookRotation(relativePos);
            
            ep.transform.SetParent(respawnPoint.transform);
            Manager.task.EnemyList[i].transform.SetParent(respawnPoint.transform);
            respawnPoint.transform.rotation = new Quaternion(rotation.x, -rotation.y, rotation.z, rotation.w);
            float normalAzimut = Mathf.Repeat(Manager.startAzimut, 360);
            respawnPoint.transform.eulerAngles = new Vector3(0, normalAzimut, 0);

            //Изменения высоты точек траектории
            if (Manager.customPathValues)
            {
                switch (en.mapPath)
                {
                    case 3:
                        SetParamsForPikirov15(ep.points);
                        break;
                    case 4:
                        Debug.Log("TURNED ON KABR");
                        SetParamsForKabr45(ep.points);
                        break;
                }
                SetKursParam(ep, Manager.kursParam);
            }
        }

        for (int i = 0, length = Manager.task.Rockets.Count; i < length; i++) {
			RocketTypes rt = Manager.task.Rockets [i];

			switch (i) {
				case 0:
					App.State.R1 = rt;
				break;
				case 1:
					App.State.R2 = rt;
				break;
				case 2:
                    App.State.R3 = rt;
				break;
				case 3:
                    App.State.R4 = rt;
				break;
			}
		}

		if (Manager.task.Fog)
        {
            RenderSettings.fog = true;
            RenderSettings.fogColor = new Color32(150, 150, 150, 0);
            RenderSettings.fogMode = FogMode.ExponentialSquared;
            RenderSettings.fogDensity = 0.0053f;

            gunClone.visorCamera.clearFlags = CameraClearFlags.SolidColor;
            gunClone.visorCamera.backgroundColor = RenderSettings.fogColor;
        } else
        {
            RenderSettings.fog = false;
        }
	}

    void SetKursParam(EnemyPath ep, float value)
    {
        float z = ep.transform.position.z + value;
        ep.transform.position = new Vector3(ep.transform.position.x, ep.transform.position.y, z);
    }

    void SetParamsForPikirov15(EPPoint[] points)
    {
        points[0].SetHeight(Manager.heightsList[0]);
        points[1].SetHeight(Manager.heightsList[0]);
        points[2].SetHeight(Manager.heightsList[1]);
        points[3].SetHeight(Manager.heightsList[1]);
        points[1].SetSpeedToPoint(Manager.speedsList[0]);

        //Вычисление скорости подъема Apache 15grad
        float z = Mathf.Abs(points[2].gameObject.transform.position.y) - Mathf.Abs(points[1].gameObject.transform.position.y);
        float angle = z / Mathf.Sqrt(z * z + 90000);
        float speed = Mathf.Sqrt(13 * 13 + 150 * 150 - 2 * 150 * 13 * Mathf.Cos(angle));

        points[2].SetSpeedToPoint(speed);
        points[3].SetSpeedToPoint(Manager.speedsList[2]);
        points[4].SetSpeedToPoint(Manager.speedsList[3]);
    }


    void SetParamsForKabr45(EPPoint[] points)
    {
        points[0].SetHeight(Manager.heightsList[0]);
        points[1].SetHeight(Manager.heightsList[0]);
        points[2].SetHeight(Manager.heightsList[0] + 300);
        points[3].SetHeight(Manager.heightsList[0] + 1000);
        points[4].SetHeight(Manager.heightsList[0] + 2600);
        points[5].SetHeight(Manager.heightsList[0] + 3600);
        points[6].SetHeight(Manager.heightsList[0] + 3400);
        points[7].SetHeight(Manager.heightsList[0] + 3150);
        points[8].SetHeight(Manager.heightsList[0] + 3150);
        points[1].SetSpeedToPoint(Manager.speedsList[0]);
        points[2].SetSpeedToPoint(Manager.speedsList[1]);
        points[3].SetSpeedToPoint(Manager.speedsList[2]);
        points[4].SetSpeedToPoint(Manager.speedsList[2]);
        points[5].SetSpeedToPoint(Manager.speedsList[2]);
        points[6].SetSpeedToPoint(Manager.speedsList[2]);
        points[7].SetSpeedToPoint(Manager.speedsList[2]);
        points[8].SetSpeedToPoint(Manager.speedsList[3]);
    }

    void OnApplicationQuit()
    {
        if (!Assets.Core.Classes.Logic.Network.IsNetworkActive())
        {
            Assets.Core.Classes.Logic.Network.CloseSocket();
        }
    }
}

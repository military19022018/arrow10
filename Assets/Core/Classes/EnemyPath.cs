﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPath : MonoBehaviour {

	public EPPoint startPoint {
		get {
			if (points.Length == 0) {
				return null;
			}

			return points [0];
		}
	}

    public EPPoint[] points;
    private int currentPoint = 0;

    void Start() {
		
		points = transform.GetComponentsInChildren<EPPoint> ();

	}
    void Update() 
    {
       
    }

    public EPPoint nextPoint(Vector3 position)
    {
        if (currentPoint >= points.Length)
        {
            return null;
        }

        //Debug.Log(Vector3.Distance(position, points[currentPoint].position));


        if (Vector3.Distance(position, points[currentPoint].position) <= 10)
        {

            currentPoint++;
            return points[currentPoint-1];
        }

        return points[currentPoint];
    }



    void OnDrawGizmos(){
		points = transform.GetComponentsInChildren<EPPoint> ();

	}		
}

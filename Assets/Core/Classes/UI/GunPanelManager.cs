﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunPanelManager : MonoBehaviour {

    public GameObject[] panels;
    int previewPanel = 0;

    public void ChangePanel(int index)
    {
        panels[previewPanel].SetActive(false);
        panels[index].SetActive(true);
        previewPanel = index;
    }
}

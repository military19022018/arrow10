﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuLayer : MonoBehaviour {

    public MenuLayer parent;

    public bool currentShow = true;

	// Use this for initialization
	void Start () {
        gameObject.SetActive(currentShow);
    }
    
    public bool back()
    {
        if(parent == null)
        {
            return false;
        }
        this.hide();
        parent.show();
        return true;
    }

    public void show()
    {
        gameObject.SetActive(true);
        currentShow = true;
    }

    public void hide()
    {
        gameObject.SetActive(false);
        currentShow = false;
    }
}

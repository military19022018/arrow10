﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlPanel.Net.Serializable
{
    [Serializable]
    public class Student
    {
        public int id { get; set; }

        public string name { get; set; }

        public string squad { get; set; }

        [NonSerialized]
        private Dictionary<string, string> task;
        public Dictionary<string, string> Task
        {
            get { return task; }
            set { task = value; }
        }

        [NonSerialized]
        private Dictionary<string, string> results;
        public Dictionary<string, string> Results
        {
            get { return results; }
            set { results = value; }
        }
    }
}

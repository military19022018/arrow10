﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlPanel.Net.Serializable
{
    [Serializable]
    public class Result
    {
        public int studentId { get; set; }

        public Dictionary<String, String> studentResult { get; set; }
    }
}

﻿using ControlPanel.Forms;
using ControlPanel.Net.Serializable;
using Newtonsoft.Json;
using Quobject.SocketIoClientDotNet.Client;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlPanel.Net
{
    public class Network
    {
        private const string SEPARATOR_TEXT_FOR_SAVING =
            "--------------------------------------------------------------------------------";
        private const int PIXELS_BETWEEN_STUDENTS = 29;
        private static Network instance;
        private int duplicateStudentsCount;
        private Socket socket;
        private int count;
        private Dictionary<Student, KeyValuePair<Button, Label>> students;
        private Repository repository;  

        private Network()
        {
            this.socket = IO.Socket("http://localhost:3000");
            this.count = 1;
            this.students = new Dictionary<Student, KeyValuePair<Button, Label>>();
            this.repository = Repository.GetInstance();
        }

        public static Network GetInstance()
        {
            if (instance == null)
            {
                instance = new Network();
            }

            return instance;
        }

        public void Run(Form form)
        {
            socket.On(Socket.EVENT_CONNECT, () =>
            {
                string status = "{\"status\":\"teacher\"}";
                socket.Emit("status", status);
            });

            socket.On("studentConnected", (data) =>
            {
                Student student = JsonConvert.DeserializeObject<Student>(data.ToString());
                if (form.InvokeRequired)
                {
                    form.Invoke((MethodInvoker)delegate
                    {
                        CreateLabelAndButtonForConnectedStudent(student, form);

                        if (students.Count >= 2)
                        {
                            Button broadcastTaskBtn = new Button();
                            broadcastTaskBtn.Location = new Point(320, 9);
                            broadcastTaskBtn.Name = "broadcastTaskBtn";
                            broadcastTaskBtn.Size = new Size(190, 23);
                            broadcastTaskBtn.TabIndex = 28;
                            broadcastTaskBtn.Text = "Задать условия для всех студентов";
                            broadcastTaskBtn.UseVisualStyleBackColor = true;
                            broadcastTaskBtn.Visible = true;
                            broadcastTaskBtn.Click += new EventHandler(broadcastTask_Click);
                            form.Controls.Add(broadcastTaskBtn);
                        }
                    });
                }
            });

            socket.On("taskCompleted", (result) =>
            {
                Result res = JsonConvert.DeserializeObject<Result>(result.ToString());
                Student student = repository.Students.FirstOrDefault(s => s.id == res.studentId);
                student.Results = res.studentResult;
                SaveResultsForStudent(student);
            });

            socket.On("disconnectedStudent", (data) =>
            {
                Student student = JsonConvert.DeserializeObject<Student>(data.ToString());
                Student stdToRemove = students.FirstOrDefault(s => s.Key.id == student.id).Key;

                if (form.InvokeRequired)
                {
                    form.Invoke((MethodInvoker)delegate
                    {

                        int firstButtonLocationY = GetFirstElementLocationY();
                        int lastButtonLocationY = GetLastElementLocationY();
                        Button button = students.FirstOrDefault(b => b.Key.id == stdToRemove.id).Value.Key;
                        Label label = students.FirstOrDefault(l => l.Key.id == stdToRemove.id).Value.Value;

                        form.Controls.Remove(button);
                        form.Controls.Remove(label);
                        students.Remove(stdToRemove);
                        count--;

                        if (students.Count == 0)
                        {
                            Button btn = form.Controls.Find("broadcastTaskBtn", false).FirstOrDefault() as Button;
                            form.Controls.Remove(btn);
                        }

                        KeyValuePair<Button, Label>[] labelsAndBtns = students.Values.ToArray();

                        if (button.Location.Y == firstButtonLocationY)
                        {
                            for (int i = 0; i < labelsAndBtns.Length; i++)
                            {
                                MoveUpButtonAndLabel(labelsAndBtns[i].Key, labelsAndBtns[i].Value);
                            }
                        }
                        else if (button.Location.Y == lastButtonLocationY)
                        { }
                        else
                        {
                            for (int i = 0; i < labelsAndBtns.Length - 1; i++)
                            {
                                if (labelsAndBtns[i + 1].Key.Location.Y - labelsAndBtns[i].Key.Location.Y > PIXELS_BETWEEN_STUDENTS)
                                {
                                    MoveUpButtonAndLabel(labelsAndBtns[i + 1].Key, labelsAndBtns[i + 1].Value);
                                }
                            }
                        }
                    });
                }
            });

            socket.On(Socket.EVENT_DISCONNECT, () =>
            {
                MessageBox.Show("Connection lost(server error)");
            });
        }

        private void setTask_Click(object sender, EventArgs e)
        {
            Button clickedBtn = sender as Button;
            Student currentStudent = students.FirstOrDefault(s => s.Value.Key.Equals(clickedBtn)).Key;
            TaskForm taskDefinationForm;
            taskDefinationForm = new TaskForm(currentStudent);
            taskDefinationForm.ShowDialog();
            if (taskDefinationForm.Task == null)
            {
                return;
            }
            currentStudent.Task = taskDefinationForm.Task;
            socket.Emit("task", JsonConvert.SerializeObject(currentStudent.id), JsonConvert.SerializeObject(taskDefinationForm.Task));
            clickedBtn.Enabled = false;
            clickedBtn.Text = "Выполняет поставленную задачу";
        }

        private string BuildResultDocumentForStudent(Student student)
        {
            StringBuilder saveResultHelper = new StringBuilder();

            saveResultHelper.Append(SEPARATOR_TEXT_FOR_SAVING);
            saveResultHelper.AppendLine();
            saveResultHelper.Append("Тест: \"Боевая работа\"");
            saveResultHelper.AppendLine();
            saveResultHelper.Append(SEPARATOR_TEXT_FOR_SAVING);
            saveResultHelper.AppendLine();
            saveResultHelper.Append("Выполнил: " + student.name);
            saveResultHelper.AppendLine();
            saveResultHelper.Append("Взвод: " + student.squad);
            saveResultHelper.AppendLine();
            saveResultHelper.Append(SEPARATOR_TEXT_FOR_SAVING);
            saveResultHelper.AppendLine();

            int counter = 0;
            foreach (var item in student.Results)
            {
                if (counter == student.Results.Count - 1)
                {
                    saveResultHelper.AppendLine();
                    saveResultHelper.Append(item.Key + "        " + item.Value);
                    break;
                }

                saveResultHelper.Append(item.Key + "        " + item.Value);
                saveResultHelper.AppendLine();
                counter++;
            }

            return saveResultHelper.ToString();
        }

        private void SaveResultsForStudent(Student student)
        {
            string result = BuildResultDocumentForStudent(student);

            string currentDate = Repository.GetInstance().Date.ToString("dd.MM.yyyy");
            string dirPath = "C:\\Users\\" + Environment.UserName + "\\Desktop\\Результаты теста 'Стрела-10 Боевая работа' "
                + currentDate + "\\" + student.squad;
            string filePath = dirPath + "\\" + student.name + ".doc";

            try
            {
                if (!System.IO.File.Exists(filePath))
                {
                    System.IO.File.WriteAllText(filePath, result);
                }
                else
                {
                    duplicateStudentsCount++;
                    filePath = dirPath + "\\" + student.name + "(" + duplicateStudentsCount + ").doc";
                    System.IO.File.WriteAllText(filePath, result);
                }
            }
            catch (System.IO.DirectoryNotFoundException)
            {
                System.IO.Directory.CreateDirectory(dirPath);
                System.IO.File.WriteAllText(filePath, result);
            }
        }

        private void CreateLabelAndButtonForConnectedStudent(Student student, Form form)
        {
            Label studentLabel = new Label();
            studentLabel.AutoSize = true;
            studentLabel.Location = new Point(12, 42 + PIXELS_BETWEEN_STUDENTS * count);
            studentLabel.Name = "label" + count;
            studentLabel.Size = new Size(35, 13);
            studentLabel.TabIndex = 15;
            studentLabel.Text = student.name + " " + student.squad + " взвод";
            studentLabel.Visible = true;
            form.Controls.Add(studentLabel);

            Button studentButton = new Button();
            studentButton.Location = new Point(320, 42 + PIXELS_BETWEEN_STUDENTS * count);
            studentButton.Name = "studentButton" + count;
            studentButton.Size = new Size(183, 23);
            studentButton.TabIndex = 28;
            studentButton.Text = "Задать условия";
            studentButton.UseVisualStyleBackColor = true;
            studentButton.Visible = true;
            studentButton.Click += new EventHandler(setTask_Click);
            form.Controls.Add(studentButton);

            students.Add(student, new KeyValuePair<Button, Label>(studentButton, studentLabel));
            repository.Students.Add(student);
            count++;
        }

        private void broadcastTask_Click(object sender, EventArgs e)
        {
            List<Student> students = new List<Student>();
            foreach (var item in this.students)
            {
                if (item.Value.Key.Enabled)
                {
                    students.Add(item.Key);
                }
            }

            if (students.Count == 0)
            {
                MessageBox.Show("Все студенты выполняют задание");
                return;
            }

            int[] studentsIds = new int[students.Count];
            for (int i = 0; i < students.Count; i++)
            {
                studentsIds[i] = students[i].id;
            }

            TaskForm form = new TaskForm(students);
            form.ShowDialog();

            if (form.Task == null)
            {
                return;
            }

            socket.Emit("broadcastTask", JsonConvert.SerializeObject(studentsIds), JsonConvert.SerializeObject(form.Task));

            foreach (var item in this.students)
            {
                if (students.Contains(item.Key))
                {
                    item.Value.Key.Enabled = false;
                    item.Value.Key.Text = "Выполняет поставленную задачу";
                }
            }
        }

        private void MoveUpButtonAndLabel(Button button, Label label)
        {
            Point temp = button.Location;
            button.Location = new Point(temp.X, temp.Y - PIXELS_BETWEEN_STUDENTS);

            temp = label.Location;
            label.Location = new Point(temp.X, temp.Y - PIXELS_BETWEEN_STUDENTS);
        }

        private int GetFirstElementLocationY()
        {
            int minY = 999;
            foreach (var item in students.Values)
            {
                if (item.Key.Location.Y < minY)
                {
                    minY = item.Key.Location.Y;
                }
            }

            return minY;
        }

        private int GetLastElementLocationY()
        {
            int maxY = 0;
            foreach (var item in students.Values)
            {
                if (item.Key.Location.Y > maxY)
                {
                    maxY = item.Key.Location.Y;
                }
            }

            return maxY;
        }
    }
}

﻿var server = require('http').createServer();
var io = require('socket.io')(server);

var teacher = {};
var students = [];
/**
 * @param {studentsCount} выдает Id студентам, ни в коем случае не писать в код 'studentsCount--' !!!
 */
var studentsCount = 0;
io.on('connection', function (socket) {
    console.log('connected  ' + socket.id);
    var user = {};

    socket.on('status', function (status) {
        var json = JSON.parse(status);
        user.status = json.status;
        console.log('status : ' + user.status);
        if (user.status === 'student') {
            socket.emit('login');
        } else {
            teacher.socket = socket;
        }
    });

    socket.on('login', function (student) {
        var student = JSON.parse(student);
        student.id = studentsCount;
        studentsCount++;
        console.log('Студент: ' + student.name + ' из ' + student.squad + '-го взвода к бою готов');
        teacher.socket.emit('studentConnected', JSON.stringify(student));
        student.socket = socket;
        students.push(student);
    });

    socket.on('task', function (studentId, task) {
        var student = findStudentById(studentId);
        student.socket.emit('task', task);
    });

    socket.on('broadcastTask', function (students, task) {
        var students = JSON.parse(students);
	console.log(students);
	while(students.length != 0) {
	    var student = findStudentById(students[0]);
	    student.socket.emit('task', task);
	    students.splice(0, 1);
	}
    });

    socket.on('taskCompleted', function (result) {
        var student = findStudentBySocket(socket);
        console.log('Студент: ' + student.name + ' закончил выполнение задания');
        var json = '{"studentId":' + student.id + ',"studentResult":' + result + '}';
        teacher.socket.emit('taskCompleted', json);
	
    });

    socket.on('disconnect', function () {
        if (socket === teacher.socket) {
            process.exit(0);
        } else {
	    var student = findStudentBySocket(socket);
	    var data = {};
            data.id = student.id;
            data.name = student.name;
            data.squad = student.squad;
            console.log('student ' + student.name + ' ' + student.squad + ' has disconnected');
            teacher.socket.emit('disconnectedStudent', JSON.stringify(data));
	    console.log(student.name + '  deleted from collections');
	    removeStudent(student);
	}
    });
});

function findStudentById(id) {
    for (i = 0; i < students.length; i++) {
        if (students[i].id == id) {
            return students[i];
        }
    }
    return null;
}

//only for clients sockets
function findStudentBySocket(socket) {
    for (i = 0; i < students.length; i++) {
        if (students[i].socket.id == socket.id) {
            return students[i];
        }
    }
    return null;
}

function showMeStudents() {
    for (i = 0; i < students.length; i++) {
        console.log(students[i].name);
    }
}

function removeStudent(student) {
    for(i = 0; i < students.length; i++) {
        if (student.id == students[i].id) {
	    students.splice(i, 1);
	}
    }
}

server.listen(3000);
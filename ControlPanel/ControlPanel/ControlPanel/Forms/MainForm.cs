﻿using ControlPanel.Forms;
using ControlPanel.Net;
using ControlPanel.Net.Serializable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlPanel
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            Network network = Network.GetInstance();
            network.Run(this);
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {

        }
    }
}

﻿namespace ControlPanel.Forms
{
    partial class TaskForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.acceptConditionBtn = new System.Windows.Forms.Button();
            this.laTypeDDList = new System.Windows.Forms.ComboBox();
            this.laTypeLbl = new System.Windows.Forms.Label();
            this.laSpeedLbl = new System.Windows.Forms.Label();
            this.azimuthLbl = new System.Windows.Forms.Label();
            this.aimDataLbl = new System.Windows.Forms.Label();
            this.aimDataDDList = new System.Windows.Forms.ComboBox();
            this.noisesLbl = new System.Windows.Forms.Label();
            this.noisesDDList = new System.Windows.Forms.ComboBox();
            this.noisesTimeDDList = new System.Windows.Forms.ComboBox();
            this.trajectoryLbl = new System.Windows.Forms.Label();
            this.trajectoryDDList = new System.Windows.Forms.ComboBox();
            this.startRangeLbl = new System.Windows.Forms.Label();
            this.heightLbl = new System.Windows.Forms.Label();
            this.startRangeTB = new System.Windows.Forms.TextBox();
            this.heightTB = new System.Windows.Forms.TextBox();
            this.weatherLbl = new System.Windows.Forms.Label();
            this.weatherDDList = new System.Windows.Forms.ComboBox();
            this.azimuthTB = new System.Windows.Forms.TextBox();
            this.laSpeedValueLbl = new System.Windows.Forms.Label();
            this.projectilesLbl = new System.Windows.Forms.Label();
            this.projectile1DDList = new System.Windows.Forms.ComboBox();
            this.projectile2DDList = new System.Windows.Forms.ComboBox();
            this.projectile3DDList = new System.Windows.Forms.ComboBox();
            this.projectile4DDList = new System.Windows.Forms.ComboBox();
            this.projectile1Label = new System.Windows.Forms.Label();
            this.projectile2Label = new System.Windows.Forms.Label();
            this.projectile3Label = new System.Windows.Forms.Label();
            this.projectile4Label = new System.Windows.Forms.Label();
            this.mapDDList = new System.Windows.Forms.ComboBox();
            this.mapLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // acceptConditionBtn
            // 
            this.acceptConditionBtn.Location = new System.Drawing.Point(381, 336);
            this.acceptConditionBtn.Name = "acceptConditionBtn";
            this.acceptConditionBtn.Size = new System.Drawing.Size(75, 23);
            this.acceptConditionBtn.TabIndex = 0;
            this.acceptConditionBtn.Text = "Готово";
            this.acceptConditionBtn.UseVisualStyleBackColor = true;
            this.acceptConditionBtn.Click += new System.EventHandler(this.acceptConditionBtn_Click);
            // 
            // laTypeDDList
            // 
            this.laTypeDDList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.laTypeDDList.Location = new System.Drawing.Point(152, 14);
            this.laTypeDDList.Name = "laTypeDDList";
            this.laTypeDDList.Size = new System.Drawing.Size(146, 21);
            this.laTypeDDList.TabIndex = 1;
            this.laTypeDDList.SelectedIndexChanged += new System.EventHandler(this.selectLAtypeDDList_SelectedIndexChanged);
            // 
            // laTypeLbl
            // 
            this.laTypeLbl.AutoSize = true;
            this.laTypeLbl.Location = new System.Drawing.Point(13, 21);
            this.laTypeLbl.Name = "laTypeLbl";
            this.laTypeLbl.Size = new System.Drawing.Size(44, 13);
            this.laTypeLbl.TabIndex = 2;
            this.laTypeLbl.Text = "Тип ЛА";
            // 
            // laSpeedLbl
            // 
            this.laSpeedLbl.AutoSize = true;
            this.laSpeedLbl.Location = new System.Drawing.Point(13, 50);
            this.laSpeedLbl.Name = "laSpeedLbl";
            this.laSpeedLbl.Size = new System.Drawing.Size(73, 13);
            this.laSpeedLbl.TabIndex = 3;
            this.laSpeedLbl.Text = "Скорость ЛА";
            // 
            // azimuthLbl
            // 
            this.azimuthLbl.AutoSize = true;
            this.azimuthLbl.Location = new System.Drawing.Point(13, 79);
            this.azimuthLbl.Name = "azimuthLbl";
            this.azimuthLbl.Size = new System.Drawing.Size(136, 13);
            this.azimuthLbl.TabIndex = 4;
            this.azimuthLbl.Text = "Начальный азимут(0-359)";
            // 
            // aimDataLbl
            // 
            this.aimDataLbl.AutoSize = true;
            this.aimDataLbl.Location = new System.Drawing.Point(13, 114);
            this.aimDataLbl.Name = "aimDataLbl";
            this.aimDataLbl.Size = new System.Drawing.Size(80, 13);
            this.aimDataLbl.TabIndex = 5;
            this.aimDataLbl.Text = "Целеуказание";
            // 
            // aimDataDDList
            // 
            this.aimDataDDList.BackColor = System.Drawing.SystemColors.Control;
            this.aimDataDDList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.aimDataDDList.Location = new System.Drawing.Point(152, 111);
            this.aimDataDDList.Name = "aimDataDDList";
            this.aimDataDDList.Size = new System.Drawing.Size(146, 21);
            this.aimDataDDList.TabIndex = 6;
            // 
            // noisesLbl
            // 
            this.noisesLbl.AutoSize = true;
            this.noisesLbl.Location = new System.Drawing.Point(13, 189);
            this.noisesLbl.Name = "noisesLbl";
            this.noisesLbl.Size = new System.Drawing.Size(102, 13);
            this.noisesLbl.TabIndex = 7;
            this.noisesLbl.Text = "Постановка помех";
            // 
            // noisesDDList
            // 
            this.noisesDDList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.noisesDDList.Location = new System.Drawing.Point(152, 186);
            this.noisesDDList.Name = "noisesDDList";
            this.noisesDDList.Size = new System.Drawing.Size(97, 21);
            this.noisesDDList.TabIndex = 8;
            this.noisesDDList.SelectedIndexChanged += new System.EventHandler(this.noisesDDList_SelectedIndexChanged);
            // 
            // noisesTimeDDList
            // 
            this.noisesTimeDDList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.noisesTimeDDList.Location = new System.Drawing.Point(255, 186);
            this.noisesTimeDDList.Name = "noisesTimeDDList";
            this.noisesTimeDDList.Size = new System.Drawing.Size(43, 21);
            this.noisesTimeDDList.TabIndex = 9;
            this.noisesTimeDDList.SelectedIndexChanged += new System.EventHandler(this.noisesTimeDDList_SelectedIndexChanged);
            // 
            // trajectoryLbl
            // 
            this.trajectoryLbl.AutoSize = true;
            this.trajectoryLbl.Location = new System.Drawing.Point(614, 80);
            this.trajectoryLbl.Name = "trajectoryLbl";
            this.trajectoryLbl.Size = new System.Drawing.Size(67, 13);
            this.trajectoryLbl.TabIndex = 10;
            this.trajectoryLbl.Text = "Траектория";
            // 
            // trajectoryDDList
            // 
            this.trajectoryDDList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.trajectoryDDList.Items.AddRange(new object[] {
            "Неманеврирующая"});
            this.trajectoryDDList.Location = new System.Drawing.Point(491, 105);
            this.trajectoryDDList.Name = "trajectoryDDList";
            this.trajectoryDDList.Size = new System.Drawing.Size(304, 21);
            this.trajectoryDDList.TabIndex = 11;
            this.trajectoryDDList.SelectedIndexChanged += new System.EventHandler(this.pathDDList_SelectedIndexChanged);
            // 
            // startRangeLbl
            // 
            this.startRangeLbl.AutoSize = true;
            this.startRangeLbl.Location = new System.Drawing.Point(488, 147);
            this.startRangeLbl.Name = "startRangeLbl";
            this.startRangeLbl.Size = new System.Drawing.Size(132, 13);
            this.startRangeLbl.TabIndex = 12;
            this.startRangeLbl.Text = "Начальная дальность Л.";
            // 
            // heightLbl
            // 
            this.heightLbl.AutoSize = true;
            this.heightLbl.Location = new System.Drawing.Point(488, 177);
            this.heightLbl.Name = "heightLbl";
            this.heightLbl.Size = new System.Drawing.Size(83, 13);
            this.heightLbl.TabIndex = 13;
            this.heightLbl.Text = "Высота полета";
            // 
            // startRangeTB
            // 
            this.startRangeTB.Location = new System.Drawing.Point(653, 140);
            this.startRangeTB.Name = "startRangeTB";
            this.startRangeTB.Size = new System.Drawing.Size(142, 20);
            this.startRangeTB.TabIndex = 14;
            // 
            // heightTB
            // 
            this.heightTB.Location = new System.Drawing.Point(653, 174);
            this.heightTB.Name = "heightTB";
            this.heightTB.Size = new System.Drawing.Size(142, 20);
            this.heightTB.TabIndex = 15;
            // 
            // weatherLbl
            // 
            this.weatherLbl.AutoSize = true;
            this.weatherLbl.Location = new System.Drawing.Point(13, 152);
            this.weatherLbl.Name = "weatherLbl";
            this.weatherLbl.Size = new System.Drawing.Size(102, 13);
            this.weatherLbl.TabIndex = 16;
            this.weatherLbl.Text = "Погодные условия";
            // 
            // weatherDDList
            // 
            this.weatherDDList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.weatherDDList.Location = new System.Drawing.Point(152, 149);
            this.weatherDDList.Name = "weatherDDList";
            this.weatherDDList.Size = new System.Drawing.Size(146, 21);
            this.weatherDDList.TabIndex = 17;
            // 
            // azimuthTB
            // 
            this.azimuthTB.Location = new System.Drawing.Point(152, 76);
            this.azimuthTB.Name = "azimuthTB";
            this.azimuthTB.Size = new System.Drawing.Size(146, 20);
            this.azimuthTB.TabIndex = 18;
            this.azimuthTB.TextChanged += new System.EventHandler(this.azimuthTB_TextChanged);
            this.azimuthTB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.azimuthTB_KeyPress);
            // 
            // laSpeedValueLbl
            // 
            this.laSpeedValueLbl.AutoSize = true;
            this.laSpeedValueLbl.Location = new System.Drawing.Point(149, 50);
            this.laSpeedValueLbl.Name = "laSpeedValueLbl";
            this.laSpeedValueLbl.Size = new System.Drawing.Size(0, 13);
            this.laSpeedValueLbl.TabIndex = 19;
            // 
            // projectilesLbl
            // 
            this.projectilesLbl.AutoSize = true;
            this.projectilesLbl.Location = new System.Drawing.Point(370, 252);
            this.projectilesLbl.Name = "projectilesLbl";
            this.projectilesLbl.Size = new System.Drawing.Size(97, 13);
            this.projectilesLbl.TabIndex = 20;
            this.projectilesLbl.Text = "Выберите ракеты";
            // 
            // projectile1DDList
            // 
            this.projectile1DDList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.projectile1DDList.Location = new System.Drawing.Point(281, 293);
            this.projectile1DDList.Name = "projectile1DDList";
            this.projectile1DDList.Size = new System.Drawing.Size(64, 21);
            this.projectile1DDList.TabIndex = 21;
            // 
            // projectile2DDList
            // 
            this.projectile2DDList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.projectile2DDList.Location = new System.Drawing.Point(351, 293);
            this.projectile2DDList.Name = "projectile2DDList";
            this.projectile2DDList.Size = new System.Drawing.Size(64, 21);
            this.projectile2DDList.TabIndex = 22;
            // 
            // projectile3DDList
            // 
            this.projectile3DDList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.projectile3DDList.Location = new System.Drawing.Point(421, 293);
            this.projectile3DDList.Name = "projectile3DDList";
            this.projectile3DDList.Size = new System.Drawing.Size(64, 21);
            this.projectile3DDList.TabIndex = 23;
            // 
            // projectile4DDList
            // 
            this.projectile4DDList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.projectile4DDList.Location = new System.Drawing.Point(491, 293);
            this.projectile4DDList.Name = "projectile4DDList";
            this.projectile4DDList.Size = new System.Drawing.Size(64, 21);
            this.projectile4DDList.TabIndex = 24;
            // 
            // projectile1Label
            // 
            this.projectile1Label.AutoSize = true;
            this.projectile1Label.Location = new System.Drawing.Point(305, 277);
            this.projectile1Label.Name = "projectile1Label";
            this.projectile1Label.Size = new System.Drawing.Size(13, 13);
            this.projectile1Label.TabIndex = 25;
            this.projectile1Label.Text = "1";
            // 
            // projectile2Label
            // 
            this.projectile2Label.AutoSize = true;
            this.projectile2Label.Location = new System.Drawing.Point(377, 277);
            this.projectile2Label.Name = "projectile2Label";
            this.projectile2Label.Size = new System.Drawing.Size(13, 13);
            this.projectile2Label.TabIndex = 26;
            this.projectile2Label.Text = "2";
            // 
            // projectile3Label
            // 
            this.projectile3Label.AutoSize = true;
            this.projectile3Label.Location = new System.Drawing.Point(446, 277);
            this.projectile3Label.Name = "projectile3Label";
            this.projectile3Label.Size = new System.Drawing.Size(13, 13);
            this.projectile3Label.TabIndex = 27;
            this.projectile3Label.Text = "3";
            // 
            // projectile4Label
            // 
            this.projectile4Label.AutoSize = true;
            this.projectile4Label.Location = new System.Drawing.Point(514, 277);
            this.projectile4Label.Name = "projectile4Label";
            this.projectile4Label.Size = new System.Drawing.Size(13, 13);
            this.projectile4Label.TabIndex = 28;
            this.projectile4Label.Text = "4";
            // 
            // mapDDList
            // 
            this.mapDDList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mapDDList.Items.AddRange(new object[] {
            "Неманеврирующая"});
            this.mapDDList.Location = new System.Drawing.Point(491, 42);
            this.mapDDList.Name = "mapDDList";
            this.mapDDList.Size = new System.Drawing.Size(304, 21);
            this.mapDDList.TabIndex = 29;
            // 
            // mapLbl
            // 
            this.mapLbl.AutoSize = true;
            this.mapLbl.Location = new System.Drawing.Point(616, 16);
            this.mapLbl.Name = "mapLbl";
            this.mapLbl.Size = new System.Drawing.Size(62, 13);
            this.mapLbl.TabIndex = 30;
            this.mapLbl.Text = "Местность";
            // 
            // TaskForm
            // 
            this.ClientSize = new System.Drawing.Size(846, 371);
            this.Controls.Add(this.mapLbl);
            this.Controls.Add(this.mapDDList);
            this.Controls.Add(this.projectile4Label);
            this.Controls.Add(this.projectile3Label);
            this.Controls.Add(this.projectile2Label);
            this.Controls.Add(this.projectile1Label);
            this.Controls.Add(this.projectile4DDList);
            this.Controls.Add(this.projectile3DDList);
            this.Controls.Add(this.projectile2DDList);
            this.Controls.Add(this.projectile1DDList);
            this.Controls.Add(this.projectilesLbl);
            this.Controls.Add(this.laSpeedValueLbl);
            this.Controls.Add(this.azimuthTB);
            this.Controls.Add(this.weatherDDList);
            this.Controls.Add(this.weatherLbl);
            this.Controls.Add(this.heightTB);
            this.Controls.Add(this.startRangeTB);
            this.Controls.Add(this.heightLbl);
            this.Controls.Add(this.startRangeLbl);
            this.Controls.Add(this.trajectoryDDList);
            this.Controls.Add(this.trajectoryLbl);
            this.Controls.Add(this.noisesTimeDDList);
            this.Controls.Add(this.noisesDDList);
            this.Controls.Add(this.noisesLbl);
            this.Controls.Add(this.aimDataDDList);
            this.Controls.Add(this.aimDataLbl);
            this.Controls.Add(this.azimuthLbl);
            this.Controls.Add(this.laSpeedLbl);
            this.Controls.Add(this.laTypeLbl);
            this.Controls.Add(this.laTypeDDList);
            this.Controls.Add(this.acceptConditionBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "TaskForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TaskForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button acceptConditionBtn;
        private System.Windows.Forms.ComboBox laTypeDDList;
        private System.Windows.Forms.Label laTypeLbl;
        private System.Windows.Forms.Label laSpeedLbl;
        private System.Windows.Forms.Label azimuthLbl;
        private System.Windows.Forms.Label aimDataLbl;
        private System.Windows.Forms.ComboBox aimDataDDList;
        private System.Windows.Forms.Label noisesLbl;
        private System.Windows.Forms.ComboBox noisesDDList;
        private System.Windows.Forms.ComboBox noisesTimeDDList;
        private System.Windows.Forms.Label trajectoryLbl;
        private System.Windows.Forms.ComboBox trajectoryDDList;
        private System.Windows.Forms.Label startRangeLbl;
        private System.Windows.Forms.Label heightLbl;
        private System.Windows.Forms.TextBox startRangeTB;
        private System.Windows.Forms.TextBox heightTB;
        private System.Windows.Forms.Label weatherLbl;
        private System.Windows.Forms.ComboBox weatherDDList;
        private System.Windows.Forms.TextBox azimuthTB;
        private System.Windows.Forms.Label laSpeedValueLbl;
        private System.Windows.Forms.Label projectilesLbl;
        private System.Windows.Forms.ComboBox projectile1DDList;
        private System.Windows.Forms.ComboBox projectile2DDList;
        private System.Windows.Forms.ComboBox projectile3DDList;
        private System.Windows.Forms.ComboBox projectile4DDList;
        private System.Windows.Forms.Label projectile1Label;
        private System.Windows.Forms.Label projectile2Label;
        private System.Windows.Forms.Label projectile3Label;
        private System.Windows.Forms.Label projectile4Label;
        private System.Windows.Forms.ComboBox mapDDList;
        private System.Windows.Forms.Label mapLbl;
    }
}
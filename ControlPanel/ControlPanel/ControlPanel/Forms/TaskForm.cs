﻿using ControlPanel.Net.Serializable;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlPanel.Forms
{
    public partial class TaskForm : Form
    {
        private Student student;
        private ICollection<Student> students;
        //flag for defination ONE student or ALL students
        private Repository repository;
        public Dictionary<string, string> Task { get; private set; }
        public TaskForm(Student student)
        {
            InitializeComponent();
            this.repository = Repository.GetInstance();
            this.Text = "Задать обстановку для студента: " + student.name;
            this.DefineDDListsWithDefaultValues();
            this.student = student;
            students = null;
        }

        public TaskForm(List<Student> students)
        {
            InitializeComponent();
            this.repository = Repository.GetInstance();
            this.Text = "Задать обстановку для всех студентов";
            this.DefineDDListsWithDefaultValues();
            this.students = students;
            student = null;
        }

        private void selectLAtypeDDList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (laTypeDDList.SelectedIndex == 0)
            {
                DefaultLA();
                return;
            }

            switch (laTypeDDList.SelectedIndex)
            {
                case 1:
                    laSpeedValueLbl.Text = "200";
                    break;

                case 2:
                    laSpeedValueLbl.Text = "68";
                    break;

                case 3:
                    laSpeedValueLbl.Text = "300";
                    break;

                case 4:
                    laSpeedValueLbl.Text = "50";
                    break;
            }
            trajectoryDDList.SelectedIndex = 0;
            trajectoryDDList.Enabled = true;
        }

        private void azimuthTB_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
        }

        private void azimuthTB_TextChanged(object sender, EventArgs e)
        {
            if (azimuthTB.Text != "")
            {
                if (Double.Parse(azimuthTB.Text) > 359.00)
                {
                    azimuthTB.Text = "359";
                }

                if (Double.Parse(azimuthTB.Text) == 0)
                {
                    azimuthTB.Text = "0";
                }
            }
        }

        private void acceptConditionBtn_Click(object sender, EventArgs e)
        {
            if (laTypeDDList.SelectedIndex == 0)
            {
                MessageBox.Show("Выберите тип ЛА");
                return;
            }
            Task = new Dictionary<string, string>();
            Task.Add("laType", laTypeDDList.SelectedItem.ToString());
            Task.Add("laSpeed", laSpeedValueLbl.Text);
            Task.Add("azimuth", Int32.Parse(azimuthTB.Text).ToString());
            Task.Add("aimData", aimDataDDList.SelectedItem.ToString());
            Task.Add("weather", weatherDDList.SelectedItem.ToString());
            Task.Add("noisesType", noisesDDList.SelectedItem.ToString());
            if (noisesTimeDDList.Enabled)
            {
                Task.Add("noisesTime", noisesTimeDDList.SelectedItem.ToString());
            }
            Task.Add("map", mapDDList.SelectedItem.ToString());
            Task.Add("tragectory", trajectoryDDList.SelectedItem.ToString());
            Task.Add("startRange", startRangeTB.Text);
            Task.Add("startHeight", heightTB.Text);
            Task.Add("projectile1", projectile1DDList.SelectedItem.ToString());
            Task.Add("projectile2", projectile2DDList.SelectedItem.ToString());
            Task.Add("projectile3", projectile3DDList.SelectedItem.ToString());
            Task.Add("projectile4", projectile4DDList.SelectedItem.ToString());
            //student.Conditions = new Dictionary<string, string>(); or foreach students set condition
            //defination a condition + close
            //if (students != null)
            //{
            //    StringBuilder sb = new StringBuilder();
            //    sb.Append("Обстановка задана для студентов:\n");
            //    foreach (var item in students)
            //    {
            //        sb.Append(item.name + "\n");
            //    }
            //    MessageBox.Show(sb.ToString());
            //}
            //else
            //{
            //    MessageBox.Show("Обстановка для студента: " + student.name + " задана");
            //}
            this.Close();
        }

        private void DefineDDListsWithDefaultValues()
        {
            //
            //setting default LA type
            //
            InitializeDDListCollection(laTypeDDList.Items, repository.DefaultLATypes);
            laTypeDDList.SelectedIndex = 0;
            //
            //setting default LA speed
            //
            laSpeedValueLbl.Text = "";
            //
            //setting default aim data
            //
            InitializeDDListCollection(aimDataDDList.Items, repository.DefaultAimDataTypes);
            aimDataDDList.SelectedIndex = 0;
            //
            //setting default azimuth value
            //
            azimuthTB.Text = "20";
            //
            //setting default weather
            //
            InitializeDDListCollection(weatherDDList.Items, repository.DefaultWeatherTypes);
            weatherDDList.SelectedIndex = 0;
            //
            //setting default noises
            //
            InitializeDDListCollection(noisesDDList.Items, repository.DefaultNoisesTypes);
            noisesDDList.SelectedIndex = 0;
            //
            //setting default noises time(disabled when noisesList index = 0)
            //
            InitializeDDListCollection(noisesTimeDDList.Items, repository.DefaultNoisesTimeTypes);
            noisesTimeDDList.SelectedIndex = 0;
            noisesTimeDDList.Enabled = false;
            //
            //
            //
            InitializeDDListCollection(mapDDList.Items, repository.DefaultMapTypes);
            mapDDList.SelectedIndex = 0;
            //
            //setting default path(disabled before selectLAtypeDDList index will be changed)
            //
            InitializeDDListCollection(trajectoryDDList.Items, repository.DefaultPathTypes);
            trajectoryDDList.SelectedIndex = 0;
            trajectoryDDList.Enabled = false;
            //
            //setting Start range
            //
            startRangeTB.Text = "9000";
            //
            //setting start height
            //
            heightTB.Text = "50";
            heightTB.Enabled = false;
            //
            //setting default Rockets
            //
            InitializeDDListCollection(projectile1DDList.Items, repository.DefaultProjectileTypes);
            InitializeDDListCollection(projectile2DDList.Items, repository.DefaultProjectileTypes);
            InitializeDDListCollection(projectile3DDList.Items, repository.DefaultProjectileTypes);
            InitializeDDListCollection(projectile4DDList.Items, repository.DefaultProjectileTypes);
            projectile1DDList.SelectedIndex = 0;
            projectile2DDList.SelectedIndex = 0;
            projectile3DDList.SelectedIndex = 0;
            projectile4DDList.SelectedIndex = 0;
        }

        private void InitializeDDListCollection(ComboBox.ObjectCollection oldCollection, ICollection<string> newCollection)
        {
            oldCollection.Clear();
            foreach (var item in newCollection)
            {
                oldCollection.Add(item);
            }
        }

        private void DefaultLA()
        {
            InitializeDDListCollection(trajectoryDDList.Items, repository.DefaultPathTypes);
            trajectoryDDList.SelectedIndex = 0;
            trajectoryDDList.Enabled = false;
            laSpeedValueLbl.Text = "";
        }

        private void noisesTimeDDList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void noisesDDList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (noisesDDList.SelectedIndex != 0)
                {
                    noisesTimeDDList.Enabled = true;
                }
                else
                {
                    noisesTimeDDList.SelectedIndex = 0;
                    noisesTimeDDList.Enabled = false;
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                InitializeDDListCollection(noisesTimeDDList.Items, repository.DefaultNoisesTimeTypes);
            }
        }

        private void pathDDList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (trajectoryDDList.SelectedIndex != 0)
                {
                    heightTB.Enabled = true;
                }
                else
                {
                    heightTB.Text = "50";
                    heightTB.Enabled = false;
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                InitializeDDListCollection(trajectoryDDList.Items, repository.DefaultPathTypes);
            }
        }

        private void TaskForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (laTypeDDList.SelectedIndex == 0)
            {
                Task = null;
            }
        }
    }
}

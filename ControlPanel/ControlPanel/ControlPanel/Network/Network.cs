﻿using ControlPanel.Forms;
using ControlPanel.Network.Serializable;
using Newtonsoft.Json;
using Quobject.SocketIoClientDotNet.Client;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlPanel.Network
{
    public class Network
    {
        private Socket socket;
        private Form form;
        private int count;
        private Dictionary<Student, KeyValuePair<Button, Label>> students;
        private Repository repository;
        private bool taskCompleted;
        public Network(Form form)
        {
            this.form = form;
            socket = IO.Socket("http://localhost:3000");
            count = 1;
            students = new Dictionary<Student, KeyValuePair<Button, Label>>();
            repository = Repository.GetInstance();
            socket.On(Socket.EVENT_CONNECT, () =>
            {
                string status = "{\"status\":\"teacher\"}";
                socket.Emit("status", status);
            });

            socket.On("studentConnected", (data) =>
            {
                Student student = JsonConvert.DeserializeObject<Student>(data.ToString());
                if (form.InvokeRequired)
                {
                    form.Invoke((MethodInvoker)delegate
                    {
                        Label studentLabel = new Label();
                        studentLabel.AutoSize = true;
                        studentLabel.Location = new Point(12, 42 + 29 * count);
                        studentLabel.Name = "label" + count;
                        studentLabel.Size = new Size(35, 13);
                        studentLabel.TabIndex = 15;
                        studentLabel.Text = student.name + " " + student.squad;
                        studentLabel.Visible = true;
                        form.Controls.Add(studentLabel);

                        Button studentButton = new Button();
                        studentButton.Location = new Point(320, 42 + 29 * count);
                        studentButton.Name = "studentButton" + count;
                        studentButton.Size = new Size(183, 23);
                        studentButton.TabIndex = 28;
                        studentButton.Text = "Задать условия";
                        studentButton.UseVisualStyleBackColor = true;
                        studentButton.Visible = true;
                        studentButton.Click += new EventHandler(setConditions_Click);

                        form.Controls.Add(studentButton);
                        students.Add(student, new KeyValuePair<Button, Label>(studentButton, studentLabel));
                        repository.Students.Add(student);
                        count++;
                    });
                }
            });

            socket.On("disconnectedStudent", (data) =>
            {
                Student student = JsonConvert.DeserializeObject<Student>(data.ToString());

                if (form.InvokeRequired)
                {
                    form.Invoke((MethodInvoker)delegate
                    {
                        Button b = students.FirstOrDefault(s => s.Key.id == student.id).Value.Key;
                        b.Text = "DISCONNECTED";
                        b.Enabled = false;
                        //TODO сдвиг лейблов и кнопок при отключении студентов
                    });
                }

            });

            socket.On("taskCompleted", (studentId) =>
            {
                //TODO : result parsing
                //Result std = JsonConvert.DeserializeObject<Result>(result.ToString());
                int id = JsonConvert.DeserializeObject<int>(studentId.ToString());
                Student student = repository.Students.FirstOrDefault(s => s.id == id);
                Button showResultsButton = students.FirstOrDefault(s => s.Key.id == id).Value.Key;
                if (form.InvokeRequired)
                {
                    form.Invoke((MethodInvoker)delegate
                    {
                        showResultsButton.Text = "Показать результат";
                        showResultsButton.Click -= setConditions_Click;
                        showResultsButton.Click += new EventHandler(showResults_Click);
                        showResultsButton.Enabled = true;
                    });
                }
            });

            socket.On(Socket.EVENT_DISCONNECT, () =>
            {
                MessageBox.Show("Connection lost(server error)");
            });
        }

        private void setConditions_Click(object sender, EventArgs e)
        {
            Button clickedBtn = sender as Button;
            Student currentStudent = students.FirstOrDefault(s => s.Value.Key.Equals(clickedBtn)).Key;
            TaskForm form;
            form = new TaskForm(currentStudent);
            form.ShowDialog();
            socket.Emit("task", JsonConvert.SerializeObject(currentStudent.id), JsonConvert.SerializeObject(form.Task));
            clickedBtn.Enabled = false;
            clickedBtn.Text = "Выполняет поставленную задачу";
        }

        private void showResults_Click(object sender, EventArgs e)
        {
            //TODO получение результата
            Button clickedBtn = sender as Button;
            Student currentStudent = students.FirstOrDefault(s => s.Value.Key.Equals(clickedBtn)).Key;
            MessageBox.Show("RESULT IS NICE, mr. " + currentStudent.name + " Squad: " + currentStudent.squad);

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlPanel.Network.Serializable
{
    [Serializable]
    public class Student
    {
        public int id { get; set; }

        public string name { get; set; }

        public string squad { get; set; }
    }
}

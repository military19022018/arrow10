﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace ControlPanel
{
    [Serializable]
    public class Config
    {
        public string nodejspath { get; set; }

        public string serverpath { get; set; }
    }
}

﻿using ControlPanel.Net.Serializable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlPanel
{
    public class Repository
    {
        private static Repository instance;

        private Repository()
        {
            //TODO : define results from file with results
            Date = DateTime.Now;
            Students = new List<Student>();
            DefaultLATypes = new List<string>()
            {
                "Выберите тип ЛА",
                "Тип A10A",
                "БПЛА",
                "F15",
                "Вертолет",
            };

            DefaultAimDataTypes = new List<string>()
            {
                "Без",
                "С АЦУ",
                "С ПРП",
            };

            DefaultWeatherTypes = new List<string>()
            {
                "Безоблачно",
                "Легкая облачность",
                "Темное небо",
                "Туман",
            };

            DefaultNoisesTypes = new List<string>()
            {
                "Без помех",
                "ЛТЦ(Гориз.)",
                "ЛТЦ(Верт.)",
            };

            DefaultNoisesTimeTypes = new List<string>()
            {
                "2s",
                "4s",
                "6s",
                "8s",
            };

            DefaultPathTypes = new List<string>()
            {
                //"Простая",
                //"Атака цели с пикирования",
                //"Атака цели с кабрирования",
                //"Атака цели с горизонтального полета",
                "Простая",
                "Усложненная",
                "Маневрирующая",
            };

            DefaultMapTypes = new List<string>()
            {
                "Земляная",
                "Пустыня",
                "Снежная",
            };

            DefaultProjectileTypes = new List<string>()
            {
                "9М37",
                "9М37М",
                "9М333",
            };
        }

        public static Repository GetInstance()
        {
            if (instance == null)
            {
                instance = new Repository();
            }

            return instance;
        }
        public DateTime Date { get; private set; }
        public ICollection<Student> Students { get; set; }
        public ICollection<string> DefaultLATypes { get; private set; }
        public ICollection<string> DefaultAimDataTypes { get; private set; }
        public ICollection<string> DefaultWeatherTypes { get; private set; }
        public ICollection<string> DefaultNoisesTypes { get; private set; }
        public ICollection<string> DefaultNoisesTimeTypes { get; private set; }
        public ICollection<string> DefaultPathTypes { get; private set; }
        public ICollection<string> DefaultMapTypes { get; private set; }
        public ICollection<string> DefaultProjectileTypes { get; private set; }
    }
}
